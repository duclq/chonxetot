<?php
function bdsEncode($string){
    if(empty($string)) return '';
    $string = base64_encode(json_encode($string));
    $string = str_replace("m","| |", $string);
    $string = str_replace("M",": :", $string);
    $string = str_replace("O","{ }", $string);
    $string = str_replace("J"," ", $string);
    return $string;
}
function bdsDecode($string){
    if(empty($string)) return '';
    $string = str_replace("| |","m", $string);
    $string = str_replace(": :","M", $string);
    $string = str_replace("{ }","O", $string);
    $string = str_replace(" ","J", $string);
    $string = json_decode(base64_decode($string),true);
    return $string;
}
function mergeKeyword($prefix,$keyword){
    $tring = $prefix;
    $arrKey = explode(" ",$prefix);
    $prefixTemp = '';
    foreach($arrKey as $key => $value){
        $temp = str_replace([",",".","_"],'',mb_strtolower($value,'UTF-8'));
        if(strpos(mb_strtolower($keyword,'UTF-8'),$temp) !== false){
            continue;
        }
        $prefixTemp .= " " . $value;
    }
    return $prefixTemp . $keyword;
}
function convertToUnicode($string){
    $trans = array("á"=>"á","à"=>"à","ả" => "ả","ã" => "ã","ạ"=> "ạ","ă" => "ă","ắ"=>"ắ" ,
        "ằ"=>"ằ" , "ẳ"=>"ẳ" , "ẵ"=>"ẵ" ,"ặ"=>"ặ" ,"â"=>"â" ,"ấ"=>"ấ" ,"ầ"=>"ầ" ,"ẩ"=>"ẩ" ,
        "ậ"=>"ậ" ,"ẫ"=>"ẫ" ,"ó"=>"ó" ,"ò"=>"ò" ,"ỏ"=>"ỏ" ,"õ"=>"õ" ,"ọ"=>"ọ" ,"ô"=>"ô" ,
        "ố"=>"ố" ,"ồ"=>"ồ" ,"ổ"=>"ổ" ,"ỗ"=>"ỗ" ,"ộ"=>"ộ" ,"ơ"=>"ơ" ,"ớ"=>"ớ" ,"ờ"=>"ờ" ,
        "ở"=>"ở" ,"ỡ"=>"ỡ" ,"ợ"=>"ợ" ,"ú"=>"ú" ,"ù"=>"ù" ,"ủ"=>"ủ" ,"ũ"=>"ũ" ,"ụ"=>"ụ" ,
        "ư"=>"ư" ,"ứ"=>"ứ" ,"ừ"=>"ừ" ,"ử"=>"ử" ,"ự"=>"ự" ,"ữ"=>"ữ" ,"é"=>"é" ,"è"=>"è" ,
        "ẻ"=>"ẻ" ,"ẽ"=>"ẽ" ,"ẹ"=>"ẹ" ,"ê"=>"ê" ,"ế"=>"ế" ,"ề"=>"ề" ,"ể"=>"ể" ,"ễ"=>"ễ" ,
        "ệ"=>"ệ" ,"í"=>"í" ,"ì"=>"ì" ,"ỉ"=>"ỉ" ,"ĩ"=>"ĩ" ,"ị"=>"ị" ,"ý"=>"ý" ,"ỳ"=>"ỳ" ,
        "ỷ"=>"ỷ" ,"ỹ"=>"ỹ" ,"ỵ"=>"ỵ" ,"đ"=>"đ" ,"Á"=>"Á" ,"À"=>"À" ,"Ả"=>"Ả" ,"Ã"=>"Ã" ,
        "Ạ"=>"Ạ" ,"Ă"=>"Ă" ,"Ắ"=>"Ắ" ,"Ằ"=>"Ằ" ,"Ẳ"=>"Ẳ" ,"Ẵ"=>"Ẵ" ,"Ặ"=>"Ặ" ,"Â"=>"Â" ,
        "Ấ"=>"Ấ" ,"Ầ"=>"Ầ" ,"Ẩ"=>"Ẩ" ,"Ậ"=>"Ậ" ,"Ẫ"=>"Ẫ" ,"Ó"=>"Ó" ,"Ò"=>"Ò" ,"Ỏ"=>"Ỏ" ,
        "Õ"=>"Õ" ,"Ọ"=>"Ọ" ,"Ô"=>"Ô" ,"Ố"=>"Ố" ,"Ồ"=>"Ồ" ,"Ổ"=>"Ổ" ,"Ỗ"=>"Ỗ" ,"Ộ"=>"Ộ" ,
        "Ơ"=>"Ơ" ,"Ớ"=>"Ớ" ,"Ờ"=>"Ờ" ,"Ở"=>"Ở" ,"Ỡ"=>"Ỡ" ,"Ợ"=>"Ợ" ,"Ú"=>"Ú" ,"Ù"=>"Ù" ,
        "Ủ"=>"Ủ" ,"Ũ"=>"Ũ" ,"Ụ"=>"Ụ" ,"Ư"=>"Ư" ,"Ứ"=>"Ứ" ,"Ừ"=>"Ừ" ,"Ử"=>"Ử" ,"Ữ"=>"Ữ" ,
        "Ự"=>"Ự" ,"É"=>"É" ,"È"=>"È" ,"Ẻ"=>"Ẻ" ,"Ẽ"=>"Ẽ" ,"Ẹ"=>"Ẹ" ,"Ê"=>"Ê" ,"Ế"=>"Ế" ,
        "Ề"=>"Ề" ,"Ể"=>"Ể" ,"Ễ"=>"Ễ" ,"Ệ"=>"Ệ" ,"Í"=>"Í" ,"Ì"=>"Ì" ,"Ỉ"=>"Ỉ" ,"Ĩ"=>"Ĩ" ,
        "Ị"=>"Ị" ,"Ý"=>"Ý" ,"Ỳ"=>"Ỳ","Ỷ"=>"Ỷ","Ỹ"=>"Ỹ","Ỵ"=>"Ỵ","Đ"=>"Đ",
        "&#225;" => "á",	"&#224;" => "à",	"&#7843;" => "ả",	"&#227;" => "ã",	"&#7841;" => "ạ",	"&#259;" => "ă",
        "&#7855;" => "ắ",	"&#7857;" => "ằ",	"&#7859;" => "ẳ",	"&#7861;" => "ẵ",	"&#7863;" => "ặ",	"&#226;" => "â",
        "&#7845;" => "ấ",	"&#7847;" => "ầ", "&#7849;" => "ẩ", "&#7853;" => "ậ", "&#7851;" => "ẫ", "&#243;" => "ó",
        "&#242;" => "ò", "&#7887;" => "ỏ", "&#245;" => "õ", "&#7885;" => "ọ", "&#244;" => "ô", "&#7889;" => "ố",
        "&#7891;" => "ồ", "&#7893;" => "ổ", "&#7895;" => "ỗ",	"&#7897;" => "ộ", "&#417;" => "ơ",	"&#7899;" => "ớ",
        "&#7901;" => "ờ",	"&#7903;" => "ở",	"&#7905;" => "ỡ",	"&#7907;" => "ợ",	"&#250;" => "ú", "&#249;" => "ù",
        "&#7911;" => "ủ",	"&#361;" => "ũ", "&#7909;" => "ụ",	"&#432;" => "ư", 	"&#7913;" => "ứ", "&#7915;" => "ừ",
        "&#7917;" => "ử",	"&#7921;" => "ự",	"&#7919;" => "ữ", "&#233;" => "é",	"&#232;" => "è", "&#7867;" => "ẻ",
        "&#7869;" => "ẽ",	"&#7865;" => "ẹ",	"&#234;" => "ê",	"&#7871;" => "ế", "&#7873;" => "ề",	"&#7875;" => "ể",
        "&#7877;" => "ễ",	"&#7879;" => "ệ", "&#237;" => "í",	"&#236;" => "ì", "&#7881;" => "ỉ", "&#297;" => "ĩ",
        "&#7883;" => "ị",	"&#253;" => "ý", "&#7923;" => "ỳ",	"&#7927;" => "ỷ",	"&#7929;" => "ỹ",	"&#7925;" => "ỵ",
        "&#273;" => "đ", "&#193;" => "Á", "&#192;" => "À",	"&#7842;" => "Ả",	"&#195;" => "Ã",	"&#7840;" => "Ạ",
        "&#258;" => "Ă","&#7854;" => "Ắ","&#7856;" => "Ằ",	"&#7858;" => "Ẳ",	"&#7860;" => "Ẵ",	"&#7862;" => "Ặ",
        "&#194;" => "Â", "&#7844;" => "Ấ",	"&#7846;" => "Ầ",	"&#7848;" => "Ẩ",	"&#7852;" => "Ậ",	"&#7850;" => "Ẫ",
        "&#211;" => "Ó","&#210;" => "Ò",	"&#7886;" => "Ỏ",	"&#213;" => "Õ","&#7884;" => "Ọ","&#212;" => "Ô",
        "&#7888;" => "Ố",	"&#7890;" => "Ồ",	"&#7892;" => "Ổ",	"&#7894;" => "Ỗ",	"&#7896;" => "Ộ",	"&#416;" => "Ơ",
        "&#7898;" => "Ớ",	"&#7900;" => "Ờ",	"&#7902;" => "Ở",	"&#7904;" => "Ỡ",	"&#7906;" => "Ợ",	"&#218;" => "Ú",
        "&#217;" => "Ù",	"&#7910;" => "Ủ",	"&#360;" => "Ũ","&#7908;" => "Ụ","&#431;" => "Ư",	"&#7912;" => "Ứ",
        "&#7914;" => "Ừ",	"&#7916;" => "Ử",	"&#7918;" => "Ữ",	"&#7920;" => "Ự",	"&#201;" => "É",	"&#200;" => "È",
        "&#7866;" => "Ẻ",	"&#7868;" => "Ẽ",	"&#7864;" => "Ẹ",	"&#202;" => "Ê",	"&#7870;" => "Ế",	"&#7872;" => "Ề",
        "&#7874;" => "Ể",	"&#7876;" => "Ễ",	"&#7878;" => "Ệ",	"&#205;" => "Í",	"&#204;" => "Ì",	"&#7880;" => "Ỉ",
        "&#296;" => "Ĩ",	"&#7882;" => "Ị",	"&#221;" => "Ý",	"&#7922;" => "Ỳ",	"&#7926;" => "Ỷ",	"&#7928;" => "Ỹ",
        "&#7924;" => "Ỵ",	"&#272;" => "Đ"
    );
    $string = strtr($string,$trans);
    $string = mb_convert_encoding($string,"UTF-8","UTF-8");
    return $string;
}
function cleanKeywordSearch($string){
    $string = convertToUnicode($string);
    $string = strval($string);
    $string = str_replace(array(chr(9),chr(10),chr(13)),"",$string);
    $string = mb_strtolower($string,"UTF-8");
    $array_bad_word = array("?", "^", ",", ";", "*", "(", ")","|", "'", "-", ">", "<", "&gt", "&lt", "=");
    $string = str_replace($array_bad_word," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = replaceSphinxMQ(replaceFCK(trim(removeHTML($string)),1));
    return trim($string);
}
function cleanKeywordSearchs($string){

    $string = convertToUnicode($string);
    $string = strval($string);
    $string = str_replace(array(chr(9),chr(10),chr(13)),"",$string);
    $string = mb_strtolower($string,"UTF-8");
    $array_bad_word = array("?", "^", ",", ";", "*", "(", ")","|", "'", "-", ">", "<", "&gt", "&lt", "=");
    $string = str_replace($array_bad_word," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  "," ",$string);
    //$string = replaceSphinxMQ(replaceFCK(trim(removeHTML($string)),1));
    return trim($string);
}
function replaceSphinxMQ($str){
    $array_bad_word = array("?", "^", ",", ";", "*", "(", ")","\\","/");
    $str	= str_replace($array_bad_word, " ", $str);
    $str	= str_replace(array("\\", "'", '"'), array("", "\\'", ""), $str);
    return $str;
}
function getImageMap($address){
    $address = urlencode($address);
    return "https://maps.googleapis.com/maps/api/staticmap?size=800x450&zoom=10&center=$address&markers=$address&format=png";
}
function generatePage($current,$total_page,$type_rewrite = "classified",$keyword = "", $show = false, $title = '', $url =''){
    $title_href = '';
    if($title != ''){
        $title_href = $title . ' - trang ';
    }
    if(!$show) return '';
    if($total_page < 2) return '';
    $min = (($current - 5) > 1) ? $current - 5 : 1;
    $max = (($min + 10) < $total_page) ? ($min + 10) : $total_page;
    $str = '<div class="pagination"><ol>';
    if($keyword != ""){

        for($i = $min; $i <= $max; $i++){
            $active = '';
            if($i == $current) $active = ' class="active"';
            $url_page = ($i > 1) ? $url . '&hpage=' . app("hashid")->encode($i) : $url;
            $str .= '<li '. $active .'><a href="' . $url_page . '" >' . $i . '</a></li>';
        }
    }else{

        switch($type_rewrite){
            case "classified":
                $rewrite = cleanRewriteAccent(getValue("rewrite","str","GET",""));
                for($i = $min; $i <= $max; $i++){
                    $active = '';
                    if($i == $current) $active = ' class="active"';
                    $url = ($i < 2) ? "/" . $rewrite : "/" . $rewrite . '/page' . app("hashid")->encode($i);

                    $str .= '<li '. $active .'><a title="'. $title_href . $i .'" href="' . $url . '" >' . $i . '</a></li>';
                }
                break;
            case "home":
                $rewrite = cleanRewriteAccent(getValue("rewrite","str","GET",""));
                for($i = $min; $i <= $max; $i++){
                    $active = '';
                    if($i == $current) $active = ' class="active"';
                    $url = '/tin-ban-xe/page' . app("hashid")->encode($i);

                    $str .= '<li '. $active .'><a href="' . $url . '" >' . $i . '</a></li>';
                }
                break;
            case "profile":
                $rewrite = cleanRewriteAccent(getValue("rewrite","str","GET",""));
                $rewrite = 'profile';

                for($i = $min; $i <= $max; $i++){
                    $active = '';
                    if($i == $current) $active = ' class="active"';
                    $url = ($i < 2) ? "/" . $rewrite : "/" . $rewrite . '/page' . app("hashid")->encode($i);

                    $str .= '<li><a href="' . $url . '" ' . $active . '>' . $i . '</a></li>';
                }
                break;
            case "review" :
                $rewrite = getValue("rewrite","str","GET","");
                for($i = $min; $i <= $max; $i++){
                    $active = '';
                    if($i == $current) $active = ' class="active"';
                    $url = ($i < 2) ? "/" . $rewrite : "/" . $rewrite . '/page' . app("hashid")->encode($i);

                    $str .= '<li '. $active .'><a href="' . $url . '" >' . $i . '</a></li>';
                }
                break;
        }
    }
    $str .= '</ol></div>';
    //_debug($_GET);
    return $str;
}
function arrayVal($array){
    if(!is_array($array)) return array();
    if(isset($array[0]) && empty($array[0]) && count($array) == 1) return array();
    return $array;
}
function showAcreage($list_acreage){
    $list_acreage = trim($list_acreage);
    if($list_acreage == ""||$list_acreage == 0) return '';
    return str_replace(",",'m<sup>2</sup>, ',$list_acreage) . 'm<sup>2</sup>';
}
function cutWord($string, $maxWord){
    $string = removeHTML($string);
    $string = replace_double_space($string);
    $string = str_replace("  ", " ",$string);
    $string = str_replace("  ", " ",$string);
    $string = str_replace("  ", " ",$string);
    $string = explode(" ",$string);
    if(count($string) <= $maxWord){
        return implode(", ",$string);
    }else{
        $string = array_slice($string,0,$maxWord);
        return implode(", ",$string);
    }
}
function getCssTypeVip(){
    $array = [1 => 'special',2 => 'supper',3=> 'vip',0 => ""];
    return $array;
}
function showAddresFromList($address){
    $address = explode(",",$address);
    $count = count($address);
    $arrayReturn = array();
    foreach($address as $key => $value){
        if($key >= ($count - 3)){
            $arrayReturn[] = '<b>' . $value . '</b>';
        }
    }
    $address = implode(",",$arrayReturn);
    return $address;
}
function showAddresFromListv2($address){
    $address = explode(",",$address);
    return $address[0] . ((count($address) > 1) ? ', ' . end($address) : '');
}
function showPriceFromList($list_price,$module = "bannhadat"){
    if(trim($list_price) == "") return '';
    $list_price = explode(",",$list_price);
    arsort($list_price);
    $endfix =  '/m<sup>2</sup>';
    if($module == "chothue") $endfix =  '';
    foreach($list_price as $key => $value){
        $value = doubleval(trim($value));
        if($value <10000){
            $list_price[$key] = 'Liên hệ';
        }elseif($value < 200000000){
            $list_price[$key] = formatPriceText($value) . $endfix;
        }else{
            $list_price[$key] = formatPriceText($value);
        }
    }
    return implode(", ",$list_price);
}
function formatPriceText($pirce){
    $pirce = doubleval($pirce);
    if($pirce < 1000000){
        return format_number(intval($pirce/1000)) . " ngàn";
    }elseif($pirce < 1000000000){
        return format_number(intval($pirce/1000000)) . " triệu";
    }elseif($pirce >= 1000000000){
        return format_number(doubleval($pirce/1000000000)) . " tỷ";
    }
}
function getUrlImageMain($array, $maxWidth = 150){
    $array = arrayVal($array);
    foreach($array as $row){
        if(isset($row['type'])){
            if($row['type'] == 'photo'){
                if(isset($row["filename"])) return getUrlPicture($row["filename"],$maxWidth);
                if(isset($row["name"])) return getUrlPicture($row["name"],$maxWidth);
            }
        }else{
            if(isset($row["filename"])) return getUrlPicture($row["filename"],$maxWidth);
            if(isset($row["name"])) return getUrlPicture($row["name"],$maxWidth);
        }
    }
    if(isset($array["filename"])) return getUrlPicture($array["filename"],$maxWidth);
    if(isset($array["name"])) return getUrlPicture($array["name"],$maxWidth);
    return config('classified.noimages', '');
}

function isHtml($string){
    if(preg_match("#<([a-zA-Z])#ui",$string)){
        return true;
    }else{
        return false;
    }
}
function getUrlImageByRow($row,$maxWidth=0,$source=false){
    $type = arrGetVal('type',$row,'photo');
    $filename = arrGetVal('filename',$row,'');
    if($filename == ''){
        $filename = arrGetVal('name',$row,'');
    }
    if($type == 'photo'){
        return getUrlPicture($filename,$maxWidth,$source);
    }
    return config('classified.noimages', '');
}
function xss_clean($data)
{
    $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
    $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

    // Remove any attribute starting with "on" or xmlns
    $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

    // Remove javascript: and vbscript: protocols
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

    // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

    // Remove namespaced elements (we do not need them)
    $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    do{
        // Remove really unwanted tags
        $old_data = $data;
        $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
    }while ($old_data !== $data);

    // we are done...
    return $data;
}
function cutSuggest($string){
    $string = explode(",",$string);
    return $string[0];
}
function checkListKeywordInText($listKeyword,$text,$getAll = false){
    $arrayReturn = [];
    $listKeyword = mb_strtolower($listKeyword,"UTF-8");
    $text = mb_strtolower($text,"UTF-8");
    $listKeyword = str_replace(["\n"],",",$listKeyword);
    $arraKey = explode(",",$listKeyword);
    foreach($arraKey as $keyword){
        $keyword = trim($keyword);
        if(mb_strlen($keyword) < 2) continue;
        if(strpos($text,$keyword) !== false){
            $arrayReturn[] = $keyword;
            if(!$getAll) return $keyword;
        }
    }
    if(!empty($arrayReturn)) return $arrayReturn;
    return false;
}
function getTitleFromText($listKeyword,$text){
    $arrayReturn = [];
    $listKeyword = mb_strtolower($listKeyword,"UTF-8");
    $text = mb_strtolower($text,"UTF-8");
    $text = str_replace(["\n",';',':','!'],".",$text);
    $arrText = explode(".",$text);
    $listKeyword = str_replace(["\n"],",",$listKeyword);
    $arraKey = explode(",",$listKeyword);
    foreach($arrText as $sen){
        $arrTemp = [];
        foreach($arraKey as $keyword){
            $keyword = trim($keyword);
            if(mb_strlen($keyword) < 2) continue;
            if(strpos($sen,$keyword) !== false){
                $arrTemp[$sen][] = $keyword;
            }
        }
        if(!empty($arrTemp)){
            $arrayReturn[] = $arrTemp;
        }
    }
    if(!empty($arrayReturn)) return $arrayReturn;
    return false;
}
function showImageString($text,$color = "#ffffff")
{
    ob_start();
    list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
    $size = 15;
    $width = mb_strlen($text,"UTF-8")*9;
    $im = imagecreate($width+1, 16);
    // White background and blue text
    $bg = imagecolorallocate($im, $r, $g, $b);
    $textcolor = imagecolorallocate($im, 0, 0, 0);

    // Write the string at the top left
    imagestring($im, 4, 0, 0, $text, $textcolor);
    imagepng($im);
    $rawImageBytes = ob_get_clean();
    imagedestroy($im);
    return "data:image/jpeg;base64," . base64_encode( $rawImageBytes ) . "";
}
function getUrlDomain(){
    $domain = $_SERVER["SERVER_NAME"] . (($_SERVER["SERVER_PORT"] == "80") ? '' : ':' . $_SERVER["SERVER_PORT"]);
    if($domain != "xe.vatgia.com" && $domain != "www.xe.vatgia.com"){
        return "http://" . $domain;
    }else{
        return "https://" . $domain;
    }
}

function arrGetVal($key, $arr,$defaultValue = ''){
    $key = explode(".",$key);
    $val = '';
    $arrTemp = $arr;
    if(empty($key)) return $defaultValue;
    foreach($key as $k){
        if(isset($arrTemp[$k])){
            $arrTemp = $arrTemp[$k];
        }else{
            $arrTemp = $defaultValue;
        }
    }
    return $arrTemp;
}

function getFileInPage($pathFile){
    if(file_exists($pathFile)){
        return file_get_contents($pathFile);
    }else{
        return '';
    }
}
function getCountWordRewrite($rewrite){
    $rewrite = explode("-",$rewrite);
    return count($rewrite);
}
function getCountWord($str){
    $str = explode(" ",$str);
    return count($str);
}
function saveLog($filename,$content){
    $path = ROOT . '\ipstore\\' . $filename;
    @file_put_contents($path,$content . "\n",FILE_APPEND);
}
function getListDomainMedia(){
    $arrayListDomainMediaVatgia = array("media.sosanhnha.com","bds.vatgia.vn","youtube.com");
    return $arrayListDomainMediaVatgia;
}
function format_login_phone($phone)
{

    $phone = str_replace('+84', '0', $phone);

    $phone = preg_replace("/[^A-Za-z0-9 ]/", '', $phone);


    //Check xem có bắt đầu bằng số 0?
    if (substr($phone, 0, 1) == '0') {
        //09 thì là 10 số --- 01 thì là 11 số
        if (
            (substr($phone, 0, 2) == '09' && strlen($phone) == 10)
            || (substr($phone, 0, 2) == '01' && strlen($phone) == 11)
            || (substr($phone, 0, 2) == '08' && strlen($phone) == 10)
        ) {
            return $phone;
        }
    }
    return false;
}
function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);
    //$clean_text = str_replace(array(chr(9)),"",$clean_text);
    $arrOtherChar = array('�');
    $clean_text = str_replace($arrOtherChar,"",$clean_text);
    return $clean_text;
}
function removeLineNull($text){
    $arrText = explode("\n",$text);
    foreach($arrText as $key => $line){
        $line = trim($line);
        if($line == "") unset($arrText[$key]);
    }
    return implode("\n",$arrText);
}
function splitPhoneNumber($string){

    /*
        Số điện thoại là các số bắt đầu == 01 -> 09 và có 10 hoặc 11 ký tự
        09: sẽ có 10 ký tự
        01 -> 08 là số máy bàn hoặc đầu số mới cũng sẽ có 10 hoặc 11 ký tự
     */
    $arrDauSo	= array(
        1 => '01',
        2 => '02',
        3 => '03',
        4 => '04',
        5 => '05',
        6 => '06',
        7 => '07',
        8 => '08',
        9 => '09'
    );

    $str_tmp = str_replace(array(" - ", " . "), " / ", $string);
    $str_tmp = preg_replace('/\s/', '', $str_tmp);

    $pattern = '/(\d{6,}(?!\d)|(?<!\d)\d{6,}|(\(|\d|\.|-|,|\)){6,})/';

    preg_match_all($pattern, $str_tmp, $match);
    //print_r($match[0]);

    $result = array();// Mang luu lai ket qua tra ve
    foreach($match[0] as $key => $value){
        // số chuẩn khi đã replace hết các ký tự string
        $phoneNumber				= preg_replace('/\D/', '', $value);
        foreach($arrDauSo as $k => $dauso){
            if(strpos($phoneNumber, $dauso) === 0){
                if($dauso == '09'){
                    if(strlen($phoneNumber) == 10){
                        $result[$key]["socu"] = removeCharPhoneNumber($value);
                        $result[$key]["somoi"] = $phoneNumber;
                    }
                }else{
                    if(strlen($phoneNumber) >= 9 && strlen($phoneNumber) <= 11){
                        $result[$key]["socu"] = removeCharPhoneNumber($value);
                        $result[$key]["somoi"] = $phoneNumber;
                    }
                }
            }
        }
    }
    $arrayReturn = array();
    foreach($result as $val){
        $val["somoi"] = format_login_phone($val["somoi"]);
        if($val["somoi"] < 10000000) continue;
        $arrayReturn[$val["somoi"]] = $val["somoi"];
    }
    //reset($arrayReturn);
    return $arrayReturn;

}
function removeCharPhoneNumber($string){

    $length  = mb_strlen($string, "UTF-8");

    $start_char = 0;
    //Remove các ký tự ko phải số ở đầu
    for($i=0; $i<$length; $i++){
        $char = mb_substr($string, $i, 1, "UTF-8");
        if(($char == "(") || (is_numeric($char))) break;
        $start_char = $i+1;
    }

    $end_char = $length;
    //Remove các ký tự ko phải số ở cuối
    for($i=$length; $i>=0; $i--){
        $char = mb_substr($string, $i-1, 1, "UTF-8");
        if(is_numeric($char)) break;
        $end_char = $i-1;
    }
    //Cắt chuỗi
    $string  = mb_substr($string, $start_char, ($end_char - $start_char), "UTF-8");

    return $string;
}
function replace_double_space($string, $char=" "){
    $i		= 0;
    $max	= 10;
    if($char == "") return $string;
    while(mb_strpos($string, $char.$char, 0, "UTF-8") !== false){
        $i++;
        $string	= str_replace($char.$char, $char, $string);
        if($i >= $max) break;
    }
    return trim($string);
}

function getCategoryType(){
    $array = array(
        'bannhadat' => array("name" => "Nhà đất bán","url" => "/" . cleanRewriteAccent("nhà đất bán"))
    ,'chothue' => array("name" => "Nhà đất cho thuê","url" => "/" . cleanRewriteAccent("Nhà đất cho thuê"))
    ,'project' => array("name" => "Dự án","url" => "/" . cleanRewriteAccent("Dự án"))
    ,'canmuacanthue' => array("name" => "Cần mua - Cần thuê","url" => "/" . cleanRewriteAccent("Cần mua - Cần thuê"))
    ,'news' => array("name" => "Tin tức","url" => "/" . cleanRewriteAccent("Tin tức"))
    ,'xaydungkientruc' => array("name" => "Xây dựng - Kiến trúc","url" => "/" . cleanRewriteAccent("Xây dựng - Kiến trúc"))
    ,'noingoaithat' => array("name" => "Nội thất - Ngoại thất","url" => "/" . cleanRewriteAccent("Nội thất - Ngoại thất"))
    ,'phongthuy' => array("name" => "Phong thủy","url" => "/" . cleanRewriteAccent("Phong thủy"))
    );
    return $array;
}

function isVietNamLanguage($string){
    if(preg_match("/[àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]/i",$string,$match)){
        return true;
    }
    return false;
}

function replaceUrlMedia($string){
    $string = str_replace("http://media.sosanhnha.com","https://media.sosanhnha.com",$string);
    return $string;
}

function myUrlEncode($url){
    $key = "vvvvvrewlrlewjrlewsssss";
    $url = str_replace("/",$key,$url);
    $url = urlencode($url);
    $url = str_replace($key,"/",$url);
    return $url;
}
function isIpDev(){
    $arrayIp = ["118.70.177.93"];
    $myIp = $_SERVER["REMOTE_ADDR"];
    if(in_array($myIp,$arrayIp)) return true;
    $admin_id = isset($_SESSION["user_id"]) ? $_SESSION["user_id"] : 0;
    if($admin_id == 1) return true;
    return false;
}

function removeScript($string){
    $string = preg_replace ('/<script.*?\>.*?<\/script>/si', '<br />', $string);
    $string = preg_replace ('/on([a-zA-Z]*)=".*?"/si', ' ', $string);
    $string = preg_replace ('/On([a-zA-Z]*)=".*?"/si', ' ', $string);
    $string = preg_replace ("/on([a-zA-Z]*)='.*?'/si", " ", $string);
    $string = preg_replace ("/On([a-zA-Z]*)='.*?'/si", " ", $string);
    return $string;
}

function clearSpaceBuffer($buffer){
    $arrStr	= array(chr(9), chr(10), chr(13).chr(13).chr(13), chr(13).chr(13));
    $arrRep	= array("", "", chr(13), chr(13));
    $buffer	= str_replace($arrStr, $arrRep, $buffer);
    return trim($buffer);
}

function rewriteNewsCar ($row){
    $title = $row["new_title"];
    $id = $row["new_id"];
    $linkNews = '/' . cleanRewriteAccent($row["new_title"]) . '-news' . $id;
    return $linkNews;
}

function getyearFromString($str = ''){
    $year = 0;
    $minYear = 1900;
    $maxYear = date('Y', time()) + 2;
    //$str = preg_match('/[0-9]{4}$/',$str);
    $arrNumber = extract_numbers($str);

    if ($arrNumber) {
        foreach ($arrNumber as $yearCheck){
            if(($yearCheck >= $minYear) && ($yearCheck <= $maxYear)) $year = $yearCheck;
        }
    }
    return intval($year);
}

function fixPriceUpdate($str_price = 0, $txt_replace = 'Giá: Liên hệ'){
    $str_price_cv = intval($str_price);
    if(!$str_price_cv) $str_price = $txt_replace;
    return $str_price;
}

function getNumberFromString($str = ''){
    $str = strval($str);
    $str = preg_replace('/\D/', '', $str);
    return $str;
}

function getDateFromString($str = ''){
    $string_containing_dates = $str;
    $matches = array();
    $timeReturn = time();

    $desired_date = '';
    preg_match('/\d{2}\/\d{2}\/\d{4}/', $string_containing_dates, $matches);

    if (isset($matches[0])) $desired_date = $matches[0];

    if($desired_date){

        $arrTime = explode("/", $desired_date);
        $day = (isset($arrTime[0])) ? $arrTime[0] : date('d', time());
        $month = (isset($arrTime[1])) ? $arrTime[1] : date('m', time());
        $year = (isset($arrTime[2])) ? $arrTime[2] : date('Y', time());

        if(checkdate($month, $day, $year)){
            $newDate = $month . '/' . $day . '/' . $year;
            $timeReturn = strtotime($newDate);
        }
    }
    return $timeReturn;
}

function getPriceFromTitile($str_price){
    $str_price = removeHTML($str_price);
    if(strpos($str_price, '<font')) {
        $str_price = substr($str_price, 0, strpos($str_price, '<font'));
    }
    $arrPrice = explode("-", $str_price);
    //_debug($arrPrice);
    $strPrice = '';
    foreach ($arrPrice as $row_price){
        if(intval($row_price) > 0) $strPrice = trim($row_price);
    }

    $strposngoac = strpos($strPrice, "(");
    if($strposngoac > 0){
        $strPrice = substr($strPrice, 0, $strposngoac);
    }
    $strPrice = trim($strPrice);
    //_debug($strPrice);
    $priceText = @$arrPrice[1];
    return $strPrice;
}

function generatePictureRaovat($name, $type = 'default', $serverName = 'http://p.vatgia.vn') {
    $picture = '';
    if ($type == 'default')
        $picture  = "$serverName/raovat_pictures/1/" . $name;
    if ($type == 'small')
        $picture  = "$serverName/raovat_pictures/1s/small_" . $name;
    if ($type == 'medium')
        $picture  = "$serverName/raovat_pictures/1m/medium_" . $name;
    if ($type == 'larger')
        $picture  = "$serverName/raovat_pictures/1l/larger_" . $name;
    return $picture;
}

function generateURL_raovat_detail($rv_name_rewrite,$iCat,$iPro){
    //global $array_url_rewrite_replace;
    $array_url_rewrite_replace = array("/","-",",","&","?","#","'",'"',"@","\\","~","(",")",".",";","*","  ","‘","’",'“','”',"%","$","^","!",":");

    //Nếu rv_name_rewrite == "" thì trình bày theo cách cũ
    if (trim($rv_name_rewrite)==""){
        $newlink = "/raovat/detail.php?iCat=" . $iCat . "&iPro=" . $iPro;
    }
    else{
        //replace các ký tự đặc biệt
        $rv_name_rewrite = str_replace($array_url_rewrite_replace," ",trim($rv_name_rewrite));
        $rv_name_rewrite = str_replace("  "," ",trim($rv_name_rewrite));

        $rv_name_rewrite = str_replace(" ","-",trim($rv_name_rewrite));
        $rv_name_rewrite = str_replace("--","-",trim($rv_name_rewrite));

        $rv_name_rewrite = urlencode(mb_strtolower($rv_name_rewrite,"UTF-8"));

        //Tạo link
        $newlink = "/raovat/" . $iCat . "/" . $iPro . "/" . $rv_name_rewrite . ".html";
    }
    return $newlink;
}



function extract_numbers($string)
{
    preg_match_all('/([\d]+)/', $string, $match);

    return $match[0];
}

function logCorePage($e){
    $strLog = '-----------' . date('d/m/Y h:s:ia') . '-----------' . PHP_EOL;
    $strLog .= $e . PHP_EOL;
    $strLog .= '----------------------------------------------------------' . PHP_EOL;
    saveLog("log_core_page.cfn",$strLog);
}

function splitAdress($strAdress = ''){
    $strReturn = $strAdress;
    $arrAdress = explode(',', $strReturn);
    $countAddress = count($arrAdress);
    if($countAddress > 3){
        $keyLast = $countAddress - 1;
        $keySpilit = intval($keyLast - 2);
        $keySpilit = ($keySpilit <= 0) ? 0 : $keySpilit;
        $arrNewAddress = [];
        for ($keySpilit; $keySpilit <= $keyLast; $keySpilit++){
            $arrNewAddress[] = $arrAdress[$keySpilit];
        }
        $strReturn = implode(',', $arrNewAddress);
        $strReturn = trim($strReturn);
        $strReturn = mb_ucfirst($strReturn, 'UTF-8');
    }
    return $strReturn;
}

function mb_ucfirst($string, $encoding)
{
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

function splitImagesNameFromUrl($url = ''){
    $imgname = '';
    if($url != ''){
        $url_arr = explode ('/', $url);
        $ct = count($url_arr);
        $name = $url_arr[$ct-1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot -1];
        $imgname = $name;
    }
    return $imgname;
}

function strposa($haystack, $needle, $offset=0) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $query) {
        if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
    }
    return false;
}

function returnDomain(){

    if(config('app.env') == 'development' && $_SERVER['SERVER_NAME'] == 'localhost') {
        //return 'http://192.168.1.141:7005/';
        return 'http://localhost:7005/';
    }else{
        return '/';
    }

}

function isDevelopment(){
    if(config('app.env') == 'development') return true;
    return false;
}

function isLocalhost(){
    if($_SERVER['SERVER_NAME'] == 'localhost') return true;
    return false;
}

function formatVehicleFeature($row = []){
    $arrReponse = [];

    if(isset($row['ved_veh_id'])) $arrReponse['id'] = $row['ved_veh_id'];
    if(isset($row['veh_id'])) $arrReponse['id'] = $row['veh_id'];

    $arrReponse['year'] = 0;
    if(isset($row['ved_year'])) $arrReponse['year'] = intval($row['ved_year']);

    if(isset($row['ved_veh_name'])) $arrReponse['name'] = $row['ved_veh_name'];
    if(isset($row['veh_name'])) $arrReponse['name'] = $row['veh_name'];

    $rewrite = '';
    if(isset($row['ved_rewrite'])) $rewrite = $row['ved_rewrite'];
    if(isset($row['veh_rewrite_sub'])) $rewrite = $row['veh_rewrite_sub'];

    $photo_vehicle = '';
    if(isset($row['ved_photo'])) $photo_vehicle = $row['ved_photo'];
    if(isset($row['veh_picture'])) $photo_vehicle = $row['veh_picture'];

    $arrReponse['link_web'] = returnDomain() . $rewrite;
    $arrReponse['link_api'] = returnDomain() . config('api.rewrites') . $rewrite;

    if($photo_vehicle != '') {
        $arrReponse['photo'] = formatDataSizeImg($photo_vehicle);
    }else{
        $arrReponse['no_photo'] = NO_PHOTO;
    }
    return $arrReponse;
}

function formatDataSizeImg($nameImg = ''){
    if($nameImg == '') return '';
    $urlPicture = [
        'xs'    => getUrlPicture($nameImg, 320),
        'sm'    => getUrlPicture($nameImg, 576),
        'md'    => getUrlPicture($nameImg, 768),
        'xl'    => getUrlPicture($nameImg)
    ];
    return $urlPicture;
}

function formatCoverVehicleYear($row = []){
    if(empty($row)) return [];
    $arrReponse = [
        'id' => $row['ved_id'],
        'year' => $row['ved_year'],
        'title_full' => $row['ved_veh_name_show'],
        'title_short' => $row['ved_veh_short_name'],
    ];
    $arrReponse['picture'] = [];
    $dataPicture = bdsDecode($row['ved_picture']);
    if(!empty($dataPicture)) {
        foreach ($dataPicture as $rowPicture){
            $arrReponse['picture'][] = formatDataSizeImg($rowPicture['name']);
        }
    }

    return $arrReponse;
}

function formatRowNews($row = []){
    if(empty($row)) return [];
    $link_api = returnDomain() . config("api.news") . $row['new_id'];
    $new_picture = $row['new_picture'];
    $arrPic = bdsDecode($new_picture);
    $photo = '';
    if(isset($arrPic['name'])) $photo = formatDataSizeImg($arrPic['name']);
    $arrReponse = [
        'id'            => $row['new_id'],
        'vehicle_id'    => $row['ncv_new_id'],
        'title'         => $row['new_title'],
        'teaser'        => $row['ned_teaser'],
        'link_web'      => returnDomain() . cleanRewriteAccent($row['new_title']).'-news'.$row['new_id'],
        'link_api'      => $link_api,
        'date'          => $row['new_date']
        //,'data'         => formatDataSizeImg($arrPic['name'])
        //'no_photo'      => NO_PHOTO,
        ,'picture'          => $photo
    ];
    if($photo != '') {
        $arrReponse['picture'] = $photo;
    }else{
        $arrReponse['no_photo'] = NO_PHOTO;
    }
    return $arrReponse;
}

function formatBrandHead($row = []){
    if(empty($row)) return [];
    if(!isset($row['veh_logo'])) return [];
    $logo = '';
    if($row['veh_logo'] != '') {
        $logo = getUrlPicture(splitImagesNameFromUrl($row['veh_logo']), 60, true);
    }
    $arrReponse = [
        'name'      => $row['rewrite']['titleRew'],
        'logo'      => $logo,
        'teaser'    => 'Tổng hợp thông tin, video, đánh giá, hình ảnh, giá <strong>'. $row['rewrite']['titleRew'] .'</strong> cập nhật mới nhất'
    ];
    return $arrReponse;
}

function formatQA($row = []){
    if(!isset($row['hd_id'])) return [];
    $arrColor = array();
    $arrColor[] = 'rgb(218, 61, 78)';
    $arrColor[] = 'rgb(205, 52, 113)';
    $arrColor[] = 'rgb(107, 73, 28)';
    $arrColor[] = 'rgb(18, 81, 245)';
    $arrColor[] = 'rgb(21, 208, 92)';

    if(empty($row)) return [];
    //mb_substr($row_hoidap['hd_poster_name'], 0, 1)
    $character = mb_strtoupper(mb_substr($row['hd_poster_name'], 0, 1));
    $arrReponse = [
        'id'            => $row['vhd_id'],
        'id_hd_vg'      => $row['hd_id'],
        'vehicle_id'    => $row['vhd_veh_id'],
        'title'         => $row['hd_name'],
        'teaser'        => $row['hd_teaser'],
        'date'          => $row['hd_post_date'],
        'link_web'      => (isset($row['vhd_rewrite']) && $row['vhd_rewrite'] != '') ? returnDomain() . $row['vhd_rewrite'] . '-' . $row['vhd_id'] : '',
        'link_api'      => returnDomain() . config('api.qa') . $row['vhd_id'],
        'user'          => [
            'user_id'   => $row['hd_user_id'],
            'user_name' => $row['hd_poster_name'],
            'key'       => $character,
            'color'     => $arrColor[array_rand($arrColor)]
        ]
        //,'data' => $row

    ];
    return $arrReponse;
}

function formatNews($row = []){
    if(empty($row)) return [];
    $ned_pictures = $row['ned_pictures'];
    $picture = [];
    if(!empty($ned_pictures)){
        $pic_decode = bdsDecode($ned_pictures);
        if(!empty($pic_encode)) {
            foreach ($pic_encode as $row_pic) {
                $picture[] = $row_pic;
            }
        }
    }
    $arrReponse = [
        'id'        => $row['new_id']
        ,'veh_id'    => $row['ncv_veh_id']
        ,'title'     => $row['new_title']
        ,'teaser'    => $row['ned_teaser']
        ,'detail'    => $row['ned_detail']
        ,'picture'   => []
    ];
    return $arrReponse;
}

function formatQaDetail($row = []){
    if(empty($row)) return [];
    $linkWeb = returnDomain();
    if(isset($row['vhd_rewrite'])) {
        $linkWeb = returnDomain() . $row['vhd_rewrite'] . '-' . $row['hd_id'];
    }
    $arrReponse = [
        'id'             => $row['hd_id']
        ,'title'         => $row['hd_name']
        ,'teaser'        => $row['hd_teaser']
        ,'description'   => $row['hd_description']
        ,'date'          => $row['hd_post_date']
        ,'user_name'     => $row['hd_poster_name']
        ,'link_web'      => $linkWeb
    ];
    return $arrReponse;
}

function formatMeta($row = []){
    $coppyRight = '©' .  date('Y', time()) . ' Xe.vatgia.com';
    $generator = 'Công ty Cổ phần Vật Giá Việt Nam.';
    $owner = 'xe.vatgia.com';
    $arrReponse = [
        [
            'name' => 'generator',
            'content' => $generator
        ]
        ,[
            'name' => 'owner',
            'content' => $owner
        ]
        ,[
            'name' => 'copyright',
            'content' => (isset($row['copyright'])) ? $row['copyright'] : $coppyRight
        ]
        ,[
            'name' => 'keywords',
            'content' => (isset($row['keyword'])) ? $row['keyword'] : ''
        ]
        ,[
            'name' => 'description',
            'content' => (isset($row['description'])) ? $row['description'] : ''
        ]
        ,[
            'name' => 'revisit-after',
            'content' => (isset($row['revisit_after'])) ? $row['revisit_after'] : ''
        ]
        ,[
            'name' => 'og:title',
            'content' => (isset($row['title'])) ? $row['title'] : ''
        ]
        ,[
            'name' => 'og:description',
            'content' => (isset($row['description'])) ? $row['description'] : ''
        ]
        ,[
            'name' => 'og:image',
            'content' => (isset($row['image'])) ? $row['image'] : ''
        ]
        ,[
            'name' => 'og:url',
            'content' => (isset($row['url'])) ? $row['url'] : ''
        ]
        ,[
            'name' => 'robots',
            'content' => (isset($row['robots'])) ? $row['robots'] : ''
        ]
        ,[
            'name' => 'google-site-verification',
            'content' => GOOGLE_SITE_VERIFICATION
        ]
        ,[
            'rel' => 'next',
            'href' => (isset($row['next'])) ? $row['next'] : ''
        ]
        ,[
            'rel' => 'prev',
            'href' => (isset($row['prev'])) ? $row['prev'] : ''
        ]

        //,'data' => $row
    ];
    if(!empty($row['geo_placename'])){
        $arrReponse[] = [
            'name' => 'geo:placename',
            'content' => $row['geo_placename']
        ];
    }
    //geo_placename
    return $arrReponse;
}

function formatSuggest($data = []){
    if(empty($data)) return [];
    $arrReponse = [];
    $arrReponse['query'] = (isset($data['query'])) ? $data['query'] : '';

    $dataSuggest = (isset($data['suggestions'])) ? $data['suggestions'] : [];
    if(!empty($dataSuggest)) {
        foreach ($dataSuggest as $key_suggest => $row_suggest){
            $arrReponse['suggestions'][$key_suggest]['title'] = $row_suggest['value'];
            $arrReponse['suggestions'][$key_suggest]['url'] = $row_suggest['url'];
        }
    }
    return $arrReponse;
}

function formatVehicleLogo($row = []){
    $logo = $row['veh_logo'];
    $logo = splitImagesNameFromUrl($logo);
    $arrLogo = formatDataSizeImg($logo);
    $arrReponse = [
        'id'        => $row['id'],
        'name'      => $row['veh_name'],
        'link_web'  => returnDomain() . $row['veh_rewrite'],
        'link_api'  => returnDomain() . config('api.rewrites') . $row['veh_rewrite'],
        'logo'      => $arrLogo
    ];
    return $arrReponse;
}


function generatePageData($current,$total_page,$type_rewrite = "classified", $base_url = '/' , $keyword = "", $show = false){
    if(!$show) return '';
    if($total_page < 2) return '';
    $min = (($current - 5) > 1) ? $current - 5 : 1;
    $max = (($min + 10) < $total_page) ? ($min + 10) : $total_page;
    $str = '<ul class="pagination">';
    $arrPage = [];
    if($keyword != ""){

        for($i = $min; $i <= $max; $i++){
            $active = '';
            if($i == $current) $active = ' class="active"';
            $url = ($i > 1) ? "/search?keyword=" . $keyword . '&page=' . $i : "/search?keyword=" . $keyword;
            $str .= '<li><a href="' . $url . '" ' . $active . '>' . $i . '</a></li>';
            $arrPage[$i] = $url;
        }
    }else{

        switch($type_rewrite){
            case "classified":

                $rewrite = cleanRewriteAccent(getValue("rewrite","str","GET",""));

                for($i = $min; $i <= $max; $i++){
                    $active = '';
                    if($i == $current) $active = ' class="active"';
                    $url = ($i < 2) ? $base_url : $base_url . '/page' . app("hashid")->encode($i);

                    $str .= '<li><a href="' . $url . '" ' . $active . '>' . $i . '</a></li>';
                    $arrPage[$i] = $url;
                }
                break;
            case "profile":
                $rewrite = cleanRewriteAccent(getValue("rewrite","str","GET",""));
                $rewrite = 'profile';

                for($i = $min; $i <= $max; $i++){
                    $active = '';
                    if($i == $current) $active = ' class="active"';
                    $url = ($i < 2) ? "/" . $rewrite : "/" . $rewrite . '/page' . app("hashid")->encode($i);

                    $str .= '<li><a href="' . $url . '" ' . $active . '>' . $i . '</a></li>';
                }
                break;

        }
    }
    $str .= '</ul>';
    //_debug($_GET);
    return $arrPage;
}

function convertDateText_v2($int_time){
    $today			=	strtotime('Today');
    $yesterday		=	strtotime('Yesterday');
    $current_time	=	time();
    $last_time		=	$current_time - $int_time;
    if($last_time < 0)
        return '';

    if($last_time < 60)
        return $last_time . ' giây trước';

    if($last_time < 3600)
        return round($last_time / 60) . ' phút trước';

    if($last_time < 86400)
        return round($last_time / 3600) . ' giờ trước';

    if($int_time > $yesterday)
        return 'Hôm qua lúc ' . date('H:i',$int_time);

    if($last_time < 86400 * 30)
        return 'Ngày '. date('d/m/Y ',$int_time);

    return date('d/m/Y',$int_time);
}

function sanitize_output($buffer) {
    $arrStr = array(chr(9), chr(10));
    $buffer = str_replace($arrStr, "", $buffer);
    $buffer = replace_double_space($buffer);
    $buffer = str_replace("> <", "><", $buffer);
    return trim($buffer);
}

function checkGoogleReferer()
{
    $UrlReferer = '';
    if (isset($_SERVER['HTTP_REFERER'])) {
        $UrlReferer = $_SERVER['HTTP_REFERER']; //get referrer
    }
    $domainRef = strtolower($UrlReferer);
    if($domainRef != '' && strpos($domainRef,"google.com") !== false){
        return true;
    }
    return false;
}

function convertPrice($txtPrice){
    $step = 10;
    $unit = 1000;
    $tram = $step * $step * $unit;
    $trieu = $tram * $step;
    $ty = $trieu * $trieu;
    $arrBase = [
        'ty' => 1000000000,
        'ti' => 1000000000,
        'trieu' => 1000000,
        'tr' => 1000000,
        'nghin' => 1000,
    ];


    $price=getValue('price', 'str', 'GET', '3,4 tỷ', 2);
    $price = $txtPrice;

    $price = trim($price);
    $price = removeAccent($price);
    $price = strtolower($price);
    $price  =str_replace(",",".",$price);
    $price  =str_replace("  "," ",$price);



    $arrPrice = explode(" ", $price);

    if(count($arrPrice)==1)
    {
        $arr1=array('ty','trieu','tram' ,'ti' ,'tr');
        $arr2=array(' ty',' trieu',' tram',' ti',' tr');
//$price=str_replace('$arr1','$arr2','$price');
        $price  =str_replace(array('ty','trieu','tram' ,'ti' ,'tr'),array(' ty ',' trieu ',' tram ',' ti ',' tr '),$price);
        echo $price;
        $arrPrice = explode(" ", $price);

    }


    $total = 0;
    foreach ($arrPrice as $key => $txtPrice) {
        if(!isset($arrBase[$txtPrice])) continue;
        $keyNumber = abs($key - 1);
        $numPrice = $arrPrice[$keyNumber];

        $typrice = $arrBase[$txtPrice];
        $total += $typrice * $numPrice;
        $count=count($arrPrice);
        //echo "So phan tu cua mang là " .$count."<br>";
        //echo "Phan tư cuối cùng trong mảng". $arrPrice[$count-1]."<br>";
        //echo "Phan tư đứng trước: ". $arrPrice[$count-2]."<br>";
        //echo "Phan tư đàu tiên: ". $arrPrice[0]."<br>";


        if(is_numeric($arrPrice[0])==false){
            print_r(is_numeric($arrPrice[0]));
            $total_dau=1*$typrice;
            //echo "Tong dau ". $total_dau."<br>";

            $tong_them=$arrPrice[$count-1]*$typrice/10;
            //echo "Tong them ". $tong_them."<br>";
            $total_all=$total_dau+$tong_them;
            //echo "Tổng thiệt hại: ". number_format($total_all);
        }
        else  if(is_numeric($arrPrice[$count-1])){
            $tong_them=$arrPrice[$count-1]*$typrice/10;
            //echo "Tong them ". $tong_them."<br>";
            $total_all=$total+$tong_them;
            // echo "Tổng thiệt hại: ". number_format($total_all);
        }

        else {
            //echo "Thanh tien :" . number_format($total);
        }
    }
    return $total;
}
function getPhoneFromParagraph($description_fake = ''){
    $description_fake = removeHTML(replaceMQ(trim($description_fake)));
    if($description_fake == '') return '';
    $str_phone = config('app.pre_phone');
    $arr_phone = explode('-',$str_phone);

    $str_pos = false;
    $arr_str = [];

    foreach ($arr_phone as $pre_phone){
        $str_pos = strpos($description_fake, $pre_phone, 0);

        //echo $str_pos . '<br>';
        if($str_pos !== false) $arr_str[] = substr($description_fake, $str_pos, 10);
        //echo $str_pos;
    }
    //echo $str_pos;
    //echo $str;
    //_debug($arr_str);
    $arr_phone = [];
    if(!empty($arr_str)){
        foreach ($arr_str as $row_phone){
            //echo getNumberFromString($row_phone) . '<br>';
            $phone_end = getNumberFromString($row_phone);
            if(strlen($phone_end) > 5) {
                $arr_phone[] = getNumberFromString($row_phone);
            }
        }
    }
    $str_phone = '';
    if(!empty($arr_phone)){

        $str_phone = implode("-", $arr_phone);
    }
    return $str_phone;
}




function chotot_get_meta($str = ''){
    $str = trim($str);
    if($str == '') return [];
    $str = strip_tags($str, '<meta>');
    //echo $str;
    $doc = new DOMDocument();
    $doc->loadHTML($str);
    $selector = new DOMXPath($doc);

    $result = $selector->query('//meta');

    $arr_data_meta = [];
    foreach($result as $node) {
        //_debug($node);
        $str_meta =  $node->getAttribute('content');
        $key_attribute = $node->getAttribute('itemprop');
        if($key_attribute == 'addressLocality') $key_attribute = 'location';
        if($key_attribute == 'addressRegion') $key_attribute = 'region';
        $arr_data_meta[$key_attribute] = utf8_decode($str_meta);
    }
    return $arr_data_meta;
}

function formatDataCrawStatics($data = []){
    $fulltime = date('Y') . '-' . date('m') . '-' . date('j');
    $arr_reponse = [
        'code' => 200,
        'data' => [
            'total' => 0,
            'sites' => []
        ],
        'params' => [
            'time' => $fulltime,
            'action' => 'get_data'
        ]
    ];

    $arr_site = [];
    $total_day = 0;
    $key_step = 0;
    foreach ($data as $row) {
        $cls_domain = $row['cls_domain'];
        if($cls_domain == '') continue;
        $cls_domain = str_replace(['https://', 'http://', '/'], '', $cls_domain);
        $day_current = date('j');
        $cls_day = 'cls_d' . $day_current;
        $arr_site[$key_step] = [
            'id'        => $row['cls_id'],
            'domain'    => $cls_domain,
        ];


        $feild_day = 0;
        if(isset($row[$cls_day])) {
            $feild_day = $row[$cls_day];
        }else{
            for($i = 1; $i <= 31; $i++){
                $cls_fix = 'cls_d' . $i;
                if(isset($row[$cls_fix])) $feild_day = $row[$cls_fix];
            }
        }

        $arr_site[$key_step]['total'] = $feild_day;

        $total_day += intval($feild_day);
        $key_step++;
    }
    $arr_reponse['data']['sites'] = $arr_site;
    $arr_reponse['data']['total'] = $total_day;
    return $arr_reponse;
}


function formatReportGoogle($data = [], $time = ''){
    if($time == '') $time = time();
    $fulltime = date('Y', $time) . '-' . date('m', $time) . '-' . date('j', $time);
    $arr_reponse = [
        'code' => 200,
        'data' => [
            'total' => 0,
            'sites' => []
        ],
        'params' => [
            'time' => $fulltime,
            'action' => 'get_data'
        ]
    ];


    $arr_site = [];
    $total_day = 0;
    $key_step = 0;
    foreach ($data as $row) {
        $cls_domain = $row['rew_title'];
        if($cls_domain == '') continue;

        $day_current = date('j');
        $cls_day = 'rew_d' . $day_current;
        $arr_site[$key_step] = [
            'id'        => $row['id'],
            'domain'    => $cls_domain,
            'total_all'     => intval(@$row['rew_gl_ref'])
        ];

        $feild_day = 0;
        if(isset($row[$cls_day])) {
            $feild_day = $row[$cls_day];
        }else{
            for($i = 1; $i <= 31; $i++){
                $cls_fix = 'cls_d' . $i;
                if(isset($row[$cls_fix])) $feild_day = $row[$cls_fix];
            }
        }

        $arr_site[$key_step]['total'] = $feild_day;

        $total_day += intval($feild_day);
        $key_step++;
    }
    $arr_reponse['data']['sites'] = $arr_site;
    $arr_reponse['data']['total'] = $total_day;
    return $arr_reponse;
}


/*BONBANH*/
function bb_split_address($str = ''){

    $address_reponse = 'Toàn quốc';

    if($str != ''){
        $str_br = strip_tags($str, '<br>');
        $arrContact = explode("<br>", $str_br);
        $address_split = '';
        $key_address = "Địa chỉ";
        $len_key = mb_strlen($key_address) + 1;
        if(!empty($arrContact)){
            foreach ($arrContact as $key_bb => $row_bb){
                $pos_adr = mb_strpos($row_bb, $key_address);
                if($pos_adr !== false) {
                    $str_len_row = mb_strlen($row_bb);
                    $address_reponse = mb_substr($row_bb, $len_key, $str_len_row);
                }
            }
        }
    }
    return $address_reponse;
}
if(!function_exists('getExtension')){
    function getExtension($filename){
        $sExtension = substr($filename, (strrpos($filename, ".") + 1));
        $sExtension = strtolower($sExtension);
        return $sExtension;
    }
}

function createKeywordSearch($str = ''){
    $str = strtolower($str);
    $str = explode(" ", $str);

    $arr_set = [];
    $arr_set_sub = [];
    $vehicle = '';
    $arr_sub = [];

    foreach ($str as $key => $row_key) {
        $row_key= str_replace(',', '', $row_key);
        if($key == 0) {
            $vehicle = $row_key;
        }else{
            $arr_sub[] = $vehicle . ' ' . $row_key;
        }
        //if(trim($row_key) == '') continue;
        $arr_set[] = trim($row_key);

    }

    $arr_key_search = [];



    if(count($arr_set)> 0){
        $arr_key_search[] = implode(',', $arr_set);
    }

    //_debug(implode(',', $arr_set));
    if(count($arr_sub)>0) {
        $arr_key_search[] = implode(',', $arr_sub);
    }
    //_debug(implode(',', $arr_sub));
    $arr_sub_s = [];

    foreach ($arr_sub as $row_sub) {
        $strKeyword = str_replace(" ", '', $row_sub);
        if(trim($strKeyword) == '') continue;
        $arr_sub_s[] = str_replace(" ", '', $row_sub);
    }

    //_debug(implode(",", $arr_sub_s));

    foreach ($arr_sub_s as $key_sub => $row_sub){

        $arr_sub_s[$key_sub] = str_replace(',', '', $row_sub);
    }
    if(!empty($arr_sub_s)){
        $arr_key_search[] = implode(',', $arr_sub_s);
    }

    $keyword_search = implode(',', $arr_key_search);

    return $keyword_search;
}

function formatAllNews($ned_detail = ''){
    $ned_detail = stripslashes(stripslashes($ned_detail));
    $ned_detail = removeScript($ned_detail);
    $ned_detail = removeLink($ned_detail);
    return $ned_detail;
}
function getGroupFbFromLink($link = ''){
    $link = str_replace('https://www.facebook.com/', '', $link);
    $link_im = explode('_', $link);
    if(empty($link_im)) return '';
    $list_gr = config('fb_group.list', []);
    $id_gr = $link_im[0];
    $gr_name = $list_gr[$id_gr];
    return $gr_name;
}

function getLogoWeb($web = ''){
    $arr_logo = config('logo_web.logo');
    $icon = (isset($arr_logo[$web])) ? $arr_logo[$web] : '';
    return $icon;
}

function getResourceCraw($link = ''){
    if(!$link) return '';
    //$link = 'https://banxehoi.com/xe-toyota-camry-hcm/can-ban-san-xuat-nam-1990-mau-do-aid1970287';
    $parse = parse_url($link);
    $host = $parse['host'];
    $ip_accept = in_array(app("user")->u_id, config('user_right'));
    if($link == '' || !$ip_accept) return '';
    $str_html = '';
    $str_html .= getLogoWeb($host);

    if(strpos($host, 'facebook') !== false){
        $str_html .= getGroupFbFromLink($link);
    }

    return '<span class="cr_sr">' . $str_html . '</span>';
}


function formatNewsCron($detail = '', $title = '')
{
    //$ned_detail = file_get_contents('data.txt');
    if (trim(removeHTML($detail)) == '') return '';
    $ned_detail = $detail;

    $html = new \App\helpers\html_cleanup($ned_detail);

    $html->setIgnoreCheckProtocol();

//gọi hàm yêu cầu xử lý và download ảnh
    $html->DOMDocument_clean_image(getListDomainMedia(), $title);
    $html->clean();
//sau khi clean la phai upload image len server api
    $ned_pictures = arrayVal($html->uploadImage(1100, 1800));
    $ned_pictures = (count($ned_pictures) < 1) ? '' : bdsEncode($ned_pictures);
    $ned_pictures = '';

    $ned_detail = replaceMQ($html->output_html);

    unset($html);

    $objhtml = new simple_html_dom($ned_detail);
    $item_rew = $objhtml->find('.prettyPhotoFrame .scroll-div');
    $arrTab = [];
    $data = '';
    foreach ($item_rew as $row_review) {
        $tittabs = $row_review->find('.tittabs', 0);

        $name_title = $tittabs->plaintext;
        $tittabs->outertext = '';
        $objhtml->save();
        $title_acent = strtolower(removeAccent($name_title));
        $title_acent = str_replace(' ', '', $title_acent);
        $content = formatAllNews($row_review->outertext);
        $arrTab[] = [
            'title' => $name_title,
            'acent' => $title_acent,
            'content' => $content
        ];
    }

    if (!empty($arrTab)) {
        $data = bdsEncode($arrTab);
    }

    //$data_reponse = bdsDecode($data);

    return $data;
}


function getVehicleFeature($rowClassified = []){
    $app_vehicle = new \App\Models\Vehicles\Vehicle();
    $arrFeature = $app_vehicle->dataFeatureValue();
    $arrReponse = [];
    $keyClassified = 0;
    $arrClaId = [];
    $arr_technical = [];

    if($rowClassified['cla_cat_id'] > 0){
        $arrClaId[$rowClassified['cla_id']] = $rowClassified['cla_cat_id'];
    }
    $arrReponse[$keyClassified] = $rowClassified;
    $cla_int11 = intval(@$rowClassified['cla_int11']);
    $arrReponse[$keyClassified]['cla_fuel'] = (isset($arrFeature[$cla_int11])) ? ucwords($arrFeature[$cla_int11]['vfv_name']) : '';
    if($arrReponse[$keyClassified]['cla_fuel'] != ''){
        $arr_technical[] = [
            'name' => 'Nhiên liệu',
            'value' => $arrReponse[$keyClassified]['cla_fuel']
        ];
    }

    $cla_int5 = intval(@$rowClassified['cla_int5']);
    $arrReponse[$keyClassified]['cla_style'] = (isset($arrFeature[$cla_int5])) ? ucwords($arrFeature[$cla_int5]['vfv_name']) : '';
    if($arrReponse[$keyClassified]['cla_style'] != ''){
        $arr_technical[] = [
            'name' => 'Dáng xe',
            'value' => $arrReponse[$keyClassified]['cla_style']
        ];
    }

    $cla_int2 = intval(@$rowClassified['cla_int2']);
    $arrReponse[$keyClassified]['cla_seconhand'] = (isset($arrFeature[$cla_int2])) ? ucwords($arrFeature[$cla_int2]['vfv_name']) : '';
    if($arrReponse[$keyClassified]['cla_seconhand'] != ''){
        $arr_technical[] = [
            'name' => 'Tình trạng',
            'value' => $arrReponse[$keyClassified]['cla_seconhand']
        ];
    }

    $cla_int1 = intval(@$rowClassified['cla_int1']);
    $arrReponse[$keyClassified]['cla_produce'] = (isset($arrFeature[$cla_int1])) ? ucwords($arrFeature[$cla_int1]['vfv_name']) : '';
    if($arrReponse[$keyClassified]['cla_produce'] != ''){
        $arr_technical[] = [
            'name' => 'Xuất xứ',
            'value' => $arrReponse[$keyClassified]['cla_produce']
        ];
    }

    $cla_int20 = intval(@$rowClassified['cla_int20']);
    $arrReponse[$keyClassified]['cla_km'] = (isset($arrFeature[$cla_int20])) ? $arrFeature[$cla_int20]['vfv_name'] : '';
    if($arrReponse[$keyClassified]['cla_km'] != ''){
        $arr_technical[] = [
            'name' => 'Số km đã đi',
            'value' => $arrReponse[$keyClassified]['cla_km']
        ];
    }

    $cla_int9 = intval(@$rowClassified['cla_int9']);
    $arrReponse[$keyClassified]['cla_tranmission'] = (isset($arrFeature[$cla_int9])) ? ucwords($arrFeature[$cla_int9]['vfv_name']) : '';
    if($arrReponse[$keyClassified]['cla_tranmission'] != ''){
        $arr_technical[] = [
            'name' => 'Hộp số',
            'value' => $arrReponse[$keyClassified]['cla_tranmission']
        ];
    }

    return $arr_technical;
}

function genMetaPageNextPrev($urlCurrent = '', $totalPage = 0){

    $pageCurrent = app("hashid")->getValue("hpage");
    if(!$pageCurrent) $pageCurrent = 1;
    $arrPageMeta = [];


    if($totalPage > 1){

        $pageNext = $pageCurrent + 1;
        $pagePrev = $pageCurrent - 1;
        $linkPageNext = $urlCurrent . '/page' . app("hashid")->encode($pageNext);
        $linkPagePrev = $urlCurrent . '/page' . app("hashid")->encode($pagePrev);
        if($pagePrev <= 1) $linkPagePrev = $urlCurrent;
        if($pageCurrent == 1) {
            $arrPageMeta = [];
            $arrPageMeta['next'] = $linkPageNext;
        }
        if($pageCurrent > 1 && $pageCurrent < $totalPage) {
            $arrPageMeta = [];
            $arrPageMeta['next'] = $linkPageNext;
            $arrPageMeta['prev'] = $linkPagePrev;
        }
        if($pageCurrent == $totalPage) {
            $arrPageMeta = [];
            $arrPageMeta['prev'] = $linkPagePrev;
        }
    }

    $str_meta = '';
    if(!empty($arrPageMeta)){
        foreach ($arrPageMeta as $rel_meta => $link_meta){
            $str_meta .= '<link rel="'. $rel_meta .'" href="'. $link_meta .'">';
        }
    }
    return $str_meta;
}


function getDataFacebook($arr = []){
    if(empty($arr)) return [];
    $linkGet = 'https://graph.facebook.com/v3.0/';
    $para = '1564910643820802/&access_token=';

    $row_get = $arr;
    $link = $linkGet . $row_get['id'] . '/' . $row_get['param'] . '&access_token=' . config('app.tokent');

    $data = file_get_contents($link);
    $result = json_decode($data,true);

    return $result;
}

function detectVehicleAI($description= ''){
    $arrdata = [
        'data' => $description
    ];
    $ch = curl_init();
    $link_curl = 'http://118.70.181.44:8010/api/predict_v2';

    curl_setopt($ch, CURLOPT_URL,$link_curl);
    curl_setopt($ch, CURLOPT_POST, 1);

    curl_setopt($ch, CURLOPT_TIMEOUT, 900);
    /*curl_setopt($ch, CURLOPT_POSTFIELDS,
        "postvar1=value1&postvar2=value2&postvar3=value3");*/

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


    curl_setopt($ch, CURLOPT_POSTFIELDS,
        http_build_query($arrdata));

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec ($ch);
    //_debug(curl_error($ch));

    curl_close ($ch);
    //_debug(json_decode($server_output, 1));
    if ($server_output == "OK") {
        //_debug($server_output);
    }else{

    }

    return $server_output;
}

function curlPostLive($arrdata = []){
    $ch = curl_init();
    $link_curl = 'http://localhost:7005/api/classified';
    if(config('app.env') != 'development') $link_curl = 'https://xe.vatgia.com/' . 'api/classified';
    //$link_curl = 'https://xe.vatgia.com/' . 'api/classified';

    curl_setopt($ch, CURLOPT_URL,$link_curl);
    curl_setopt($ch, CURLOPT_POST, 1);

    curl_setopt($ch, CURLOPT_TIMEOUT, 900);
    /*curl_setopt($ch, CURLOPT_POSTFIELDS,
        "postvar1=value1&postvar2=value2&postvar3=value3");*/

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


    curl_setopt($ch, CURLOPT_POSTFIELDS,
        http_build_query($arrdata));

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec ($ch);
    //_debug(curl_error($ch));

    curl_close ($ch);
    //_debug(json_decode($server_output, 1));
    if ($server_output == "OK") {
        //_debug($server_output);
    }else{

    }

}

function formatClassifiedsData($list_classifieds = []){
    //_debug($list_classifieds);

    // TEMP
    /*$arr_data[] = [
        'title' => str_replace("?", "", $row['dcl_title']),
        'link' => cleanRewriteAccent($row['dcl_title']) . '-' . $row['dcl_id'] . '.html',
        'teaser' => cut_string(removeHTML($row['dcl_description']), 130),
        'price' => $row['dcl_price'],
        'price_text' => formatPriceText($row['dcl_price']),
        'region' => $row['dcl_region'],
        'contact' => [
            'name' => $row['dcl_contact_name'],
            'phone' => $row['dcl_contact_phone'],
            'date' => $row['dcl_date'],
        ],
        'technical' => bdsDecode($row['dcl_data_technical']),
        'thumb' => $img[0],
        'photo' => $img
    ];*/

    $arr_technical = [
        '_3J9IfJCUXTV7Nne-3wMbVG'   => [
            'code' => 'brand',
            'name' => 'Hãng'
            ,'value' => ''
        ],
        'Pv3_Dz1t8np10JwKkJsiA'     => [
            'code' => 'model',
            'name' => 'Xe'
            ,'value' => ''
        ],
        '_9HAKKMYgqH6qx3AqwapC4'    => [
            'code' => 'productionDate',
            'name' => 'Năm Sx'
            ,'value' => ''
        ],
        'I0-3o28StAgFg3xkplTX9'     => [
            'code' => '',
            'name' => 'Km đã đi'
            ,'value' => ''
        ],
        '_37LAHfRw2Bu6XcBkTrskHM'   => [
            'code' => '',
            'name' => 'Tình trạng'
            ,'value' => ''
        ],
        '_3WJqImUjHoAAGCSpNa3aln'   => [
            'code' => '',
            'name' => 'Hộp số'
            ,'value' => ''
        ],
        '_14BvoidU0Mv0NofUMn5Oa9'   => [
            'code' => '',
            'name' => 'Nhiên liệu'
            ,'value' => ''
        ],
        '_14aKF2_vir3C5PsPiyA_Ca'   =>[
            'code' => '',
            'name' => 'Nhập khẩu'
            ,'value' => ''
        ],
        'GvKmVbpwnqqbp68z05Gz4'     => [
            'code' => '',
            'name' => 'Dáng xe'
            ,'value' => ''
        ],
        '_3KgU8ZIfBF3n6SERQe-dRH'   => [
            'code' => 'vehicleSeatingCapacity',
            'name' => 'Số ghế'
            ,'value' => ''
        ]
    ];



    $arr_tech_compare = [
        'cla_produce'   => '_14aKF2_vir3C5PsPiyA_Ca',
        'cla_seconhand' => '_3WJqImUjHoAAGCSpNa3aln',
        'cla_fuel'      => '_14BvoidU0Mv0NofUMn5Oa9',
        'cla_style'     => 'GvKmVbpwnqqbp68z05Gz4'
    ];

    $arr_use = [
        9   => 'car_new',
        10  => 'car_used'
    ];

    $arr_reponse = [];

    if(isset($list_classifieds['view'])) unset($list_classifieds['view']);
    foreach ($list_classifieds as $row){

        $arr_technical_new = $arr_technical;
        $arr_technical_new['_14aKF2_vir3C5PsPiyA_Ca']['value'] = (isset($row['cla_produce'])) ? $row['cla_produce'] : '';
        $arr_technical_new['_3WJqImUjHoAAGCSpNa3aln']['value'] = (isset($row['cla_seconhand'])) ? $row['cla_seconhand'] : '';
        $arr_technical_new['_14BvoidU0Mv0NofUMn5Oa9']['value'] = (isset($row['cla_fuel'])) ? $row['cla_fuel'] : '';
        $arr_technical_new['GvKmVbpwnqqbp68z05Gz4']['value'] = (isset($row['cla_style'])) ? $row['cla_style'] : '';

        $arr_technical_show = [];
        $arr_contact = [];
        $fullname   = '';
        //$address    = 'Đang cập nhật';
        $phone      = '';
        if(isset($row['cla_techincal_param'])){
            $arr_technical_new = bdsDecode($row['cla_techincal_param']);
            $arr_technical_show = arrGetVal('tech_show.data', $arr_technical_new);
            $arr_contact = arrGetVal('seller.data', $arr_technical_new, []);

            $fullname   = arrGetVal('fullname', $arr_contact, []);
            $address    = arrGetVal('address', $arr_contact, '');
            $phone      = arrGetVal('phone', $arr_contact, []);
        }

        if(isset($address) && $address == '') {
            $address = arrGetVal('cla_address', $row, 'Đang cập nhật');
        }else{
            $address = '';
        }



        $price_integer = (doubleval(@$row['cla_price']));
        $price_text = (isset($row['cla_price_text'])) ? $row['cla_price_text'] : 'Liên hệ';
        if($price_integer <= 0){
            $price_integer = convertPrice($price_text);
        }
        $img = [];
        if(isset($row['cla_picture'])){
            $img = bdsDecode($row['cla_picture']);
        }
        $thumb = BASE_URL . '/assets/images/noimage_2.jpg';
        $small = BASE_URL . '/assets/images/noimage_2.jpg';
        if(!empty($img)){
            if(isset($img[0]['name'])){
                $img_first = $img[0]['name'];
                $thumb = getUrlPicture($img_first, 200);
                $small = getUrlPicture($img_first, 50);
            }
        }

        $arr_reponse[] = [
            'title' => str_replace("?", "", $row['cla_title']),
            'link' => BASE_URL . '/' . $row['cla_rewrite'],
            'teaser' => cut_string(removeHTML($row['cla_teaser']), 130),
            'price' => $price_integer,
            'price_text' => $price_text,
            'region' => $address,
            'contact' => [
                'name' => ($row['cla_contact_name'] == '') ? $fullname : $row['cla_contact_name'],
                'phone' => (trim($row['cla_phone']) == false) ? $phone : $row['cla_phone'],
                'date' => $row['cla_date'],
            ],
            'status' => (isset($row['cla_int2']) && isset($arr_use[$row['cla_int2']])) ? $arr_use[$row['cla_int2']] : 'car_used',
            'technical' => $arr_technical_show,
            'vip'       => intval(@$row['cla_type_vip']),
            'thumb' => $thumb,
            'small' => $small,
            'brand' => (isset($row['cla_brand'])) ? $row['cla_brand'] : null,
            'photo' => $img
        ];

        unset($arr_technical_new);
    }

    return $arr_reponse;
}

/**
 * Function get extention image
 */
function getExtention($url = ''){
    $arrayExtention	= array('.jpg', '.jpeg', '.png', '.gif', '.bmp', '.jpe');
    $extention	= '';
    $filename	= '';
    $url			= strtolower($url);
    foreach($arrayExtention as $k => $exten){
        $position	= strripos($url, $exten);
        if($position > 0){
            $extention	= $exten;
            break;
        }
    }

    for($i=0; $i<3; $i++){
        $filename .= chr(rand(97,122));
    }
    $filename	.= time();
    $filename	.= $extention;

    return $extention;
}


function restyleTitle($title = '', $txt = "TẠI TOÀN QUỐC", $txt_r = "TOÀN QUỐC"){
    $title = str_replace($txt, $txt_r, $title);
    return $title;
}

function priceLarge($price = 0, $unit = 1000000){
    $price = intval($price) * $unit;
    return doubleval($price);
}


// Dựa vào giá đưa ra loại giá, sql
function dataTypePrice($price = 0, $feild = 'cla_price'){
    $price = doubleval($price);
    $type_price = 1;

    $arr_reponse = [
        'label' => '',
        'type'  => 0,
        'sql'   => '',
        'data'  => []
    ];
    switch ($price){
        case ($price < priceLarge(300)):
            $type_price = 1;
            break;
        case ($price >= priceLarge(300) && $price < priceLarge(400)):
            $type_price = 2;
            break;
        case ($price >= priceLarge(400) && $price < priceLarge(600)):
            $type_price = 3;
            break;
        case ($price >= priceLarge(600) && $price < priceLarge(800)):
            $type_price = 4;
            break;
        case ($price >= priceLarge(800) && $price < priceLarge(1000)):
            $type_price = 5;
            break;
        case ($price >= priceLarge(1000) && $price < priceLarge(1500)):
            $type_price = 6;
            break;
        case ($price >= priceLarge(1500) && $price < priceLarge(2000)):
            $type_price = 7;
            break;
        case ($price >= priceLarge(2000)):
            $type_price = 8;
            break;
    }

    $arr_reponse['type'] = $type_price;

    $arr_config_price = config('classified.price_range');
    $data_range= $arr_config_price[$type_price];

    $arr_reponse['label'] = $data_range['name'];

    $value_price = arrGetVal('value', $data_range, []);
    $sql = '';

    if(!empty($value_price)){
        $symbol = arrGetVal('symbol', $value_price, '');
        $min = arrGetVal('min', $value_price, '');
        $min = doubleval($min);
        $min = priceLarge($min);

        $max = arrGetVal('max', $value_price, '');
        $max = doubleval($max);
        $max = priceLarge($max);

        if($min > 0 || $max > 0){
            switch ($symbol){
                case "<":
                    $sql = $feild . "<" . $max;
                    break;
                case ">":
                    $sql = $feild . ">" . $min;
                    break;
                default:
                    $sql = $feild . ">=" . $min . ' AND ' . $feild . '<' . $max;
                    break;
            }
        }
    }
    $arr_reponse['sql']     = $sql;
    $arr_reponse['data']    = $value_price;


    return $arr_reponse;
}

// Chỉ lấy ra loại giá
function returnTypePrice($price = 0){
    $data_price = dataTypePrice($price);
    $type = intval($data_price['type']);
    return $type;
}


function genSqlSphnixLocation($str){
    $str = removeAccent($str);
    $str = strtolower($str);
    $str = trim(removeHTML($str));

    $arr_sphinix = '';

    if($str == '') return '';

    $arr_sphinix = explode(",", $str);
    if(empty($arr_sphinix) || !is_array($arr_sphinix)) return '';

    $arrSql = [];
    foreach ($arr_sphinix as $key_r => $row_r){
        $row_r = trim($row_r);
        $arr_sphinix[$key_r] = $row_r;
        $arrSql[] = "SELECT * FROM xehoi_cities WHERE MATCH ('@(cou_name,cou_short_name,cou_alias)\"" . replaceSphinxMQ($row_r) . "\"/2') ORDER BY WEIGHT() DESC LIMIT 1;";
    }
    $sql = implode("\n",$arrSql);
    return $sql;
}

function detectTypeNumber($str = ''){
    $type_content = 0;
    $str = trim($str);
    if($str != ''){
        $str = removeAccent($str);
        $str = removeEmoji($str);
        $str = removeHTML($str);
        $str = removeScript($str);
        $str = preg_replace('/[^\p{L}\p{N}\s]/u', '', $str);

        $arr_content = explode("\n", $str);

        $num_character = 0;
        foreach ($arr_content as $row_str) {
            $row_str = trim($row_str);
            $num_character += str_word_count($row_str);
        }

        switch ($num_character){
            case ($num_character >= 500 AND $num_character <= 700):
                $type_content = 1;
                break;
            case ($num_character > 700 AND $num_character <= 1000):
                $type_content = 2;
                break;
            case ($num_character > 1000):
                $type_content = 3;
                break;
            default:
                $type_content = 0;
                break;

        }
    }
    return $type_content;
}

function getPhoneFromString_v2($paragraph = ''){
    $str_phone = config('app.pre_phone');
    $arr_phone_base = explode('-',$str_phone);
    $arr_phone_base = array_combine($arr_phone_base, $arr_phone_base);

    $str = removeAccent($paragraph);
    $str = removeEmoji($str);
    $str = removeHTML($str);
    $str = removeScript($str);
    $str = preg_replace('/[^\p{L}\p{N}\s]/u', '', $str);
    $str = str_replace("  ", " ", $str);
    if($str == '') return '';
    $arr_str = explode(" ", $str);
    if(empty($arr_str)) return '';
    $arr_phone = [];
    foreach ($arr_str as $row){
        $row = getNumberFromString($row);
        $number = doubleval($row);
        if($number > 0 && strlen($number) > 5) {
            $full_number = "0" . $number;
            $firt_phone = substr($full_number, 0, 3);
            $second_phone = substr($full_number, 0, 4);
            echo $second_phone . '<br>';
            if(isset($arr_phone_base[$firt_phone]) || isset($arr_phone_base[$second_phone])) {
                $arr_phone[$number] = $full_number;
            }
            //$arr_phone[$number] = "0" . $number;
        }
    }
    return $arr_phone;
}