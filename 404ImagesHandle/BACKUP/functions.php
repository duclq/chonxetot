<?
function showimage($path){
   $file_extension = get_extension($path);
   switch( $file_extension ) {
       case "gif": $ctype="image/gif"; break;
       case "png": $ctype="image/png"; break;
       case "jpeg":
       case "jpg": $ctype="image/jpeg"; break;
       default:
         $ctype="image/jpeg"; break;
       break;
   }
   header('Content-type: ' . $ctype);
   readfile($path);
}
function bdsDecode($string){
   $string = str_replace("| |","m", $string);
   $string = str_replace(": :","M", $string);
   $string = str_replace("{ }","O", $string);
   $string = str_replace(" ","J", $string);
   $string = json_decode(base64_decode($string),true);    
   return $string;
}

function get_extension($file_name){
	$sExtension = mb_substr($file_name, (mb_strrpos($file_name, ".", 0, "UTF-8") + 1), 10, "UTF-8");
	$sExtension = mb_strtolower($sExtension, "UTF-8");
	return $sExtension;
}

function generate_file_name($file_name,$time, $nData=""){
	$file_name = explode("?",$file_name);
	$file_name = $file_name[0];
	$name = $time;
	if($nData != "") $name	.= "-" . replace_rewrite_url($nData);
   $name .= "-picture" . rand(1000, 9999);
	$ext	= get_extension($file_name);
	return mb_strtolower($name . "." . $ext, "UTF-8");
}

function check_extension($file_name, $allowList){
	$sExtension = get_extension($file_name);
	$allowArray	= explode(",", $allowList);
	$allowPass	= false;
	for($i=0; $i<count($allowArray); $i++){
		if($sExtension == trim($allowArray[$i])) $allowPass = true;
	}
	return $allowPass;
}

function replace_rewrite_url($string, $rc="-", $urlencode=1){
	$string	= mb_strtolower($string, "UTF-8");
	$string	= removeAccent($string);
	$string	= str_replace(".", "", $string);
	$string	= preg_replace('/[^A-Za-z0-9]+/', ' ', $string);
	$string = str_replace("  "," ",$string);
	$string = str_replace("  "," ",$string);
	$string = str_replace("  "," ",$string);
	$string = str_replace("  "," ",$string);
	$string	= str_replace(" ", $rc, $string);
	$string = str_replace("  "," ",$string);
	$string = str_replace("  "," ",$string);
	$string = str_replace("  "," ",$string);
	$string = str_replace("  "," ",$string);
   $string = str_replace(" ","-",$string);
	if($urlencode == 1) $string	= urlencode($string);
	return $string;
}

function removeAccent($mystring){
	$marTViet=array(
		// Chữ thường
		"à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
		"è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
		"ì","í","ị","ỉ","ĩ",
		"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
		"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
		"ỳ","ý","ỵ","ỷ","ỹ",
		"đ","Đ","'",
		// Chữ hoa
		"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
		"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
		"Ì","Í","Ị","Ỉ","Ĩ",
		"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
		"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
		"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
		"Đ","Đ","'"
		);
	$marKoDau=array(
		/// Chữ thường
		"a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
		"e","e","e","e","e","e","e","e","e","e","e",
		"i","i","i","i","i",
		"o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
		"u","u","u","u","u","u","u","u","u","u","u",
		"y","y","y","y","y",
		"d","D","",
		//Chữ hoa
		"A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A",
		"E","E","E","E","E","E","E","E","E","E","E",
		"I","I","I","I","I",
		"O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O",
		"U","U","U","U","U","U","U","U","U","U","U",
		"Y","Y","Y","Y","Y",
		"D","D","",
		);
	return str_replace($marTViet, $marKoDau, $mystring);
}

function getValue($value_name, $data_type = "int", $method = "GET", $default_value = 0, $advance = 0){
	$value = $default_value;
	switch($method){
		case "GET": if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
		case "POST": if(isset($_POST[$value_name])) $value = $_POST[$value_name]; break;
		case "COOKIE": if(isset($_COOKIE[$value_name])) $value = $_COOKIE[$value_name]; break;
		case "SESSION": if(isset($_SESSION[$value_name])) $value = $_SESSION[$value_name]; break;
		default: if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
	}
	/**
	 * Edit 26/03/2014
	 * - Sửa lại để không dính lỗi trên PHP 5.4 với hàm strval khi get arr
	 */
	$data_type = trim(strtolower($data_type));
	switch($data_type)
	{
		case 'int':
         $value = str_replace('.', '', $value);
			$returnValue = intval($value);
			break;
		case 'str':
			$returnValue = strval($value);
			break;
		case 'flo':
			$returnValue = floatval($value);
			break;
		case 'dbl':
			$returnValue = doubleval($value);
			break;
		case 'arr':
			$returnValue = $value;
			break;
		default:
			//Nếu mặc định ko truyền data_type thì là kiểu int
			$returnValue = intval($value);
			break;
	}
	//Check xem có cần format giá trị trả về hay không??
	if($advance != 0 && is_string($returnValue)){
		switch($advance){
			case 1:
				$returnValue = replaceMQ($returnValue);
				break;
			case 2:
				$returnValue = htmlspecialbo($returnValue);
				break;
		}
	}
	//Do số quá lớn nên phải kiểm tra trước khi trả về giá trị
	if(($data_type != "str") && !is_array($returnValue) && (strval($returnValue) == "INF")) return 0;
	return $returnValue;
	/*
     $valueArray	= array("int" => intval($value), "str" => trim(strval($value)), "flo" => floatval($value), "dbl" => doubleval($value), "arr" => $value);
     foreach($valueArray as $key => $returnValue){
         if($data_type == $key){
             if($advance != 0){
                 switch($advance){
                     case 1:
                         $returnValue = replaceMQ($returnValue);
                         break;
                     case 2:
                         $returnValue = htmlspecialbo($returnValue);
                         break;
                 }
             }
             //Do số quá lớn nên phải kiểm tra trước khi trả về giá trị
             if((strval($returnValue) == "INF") && ($data_type != "str")) return 0;
             return $returnValue;
             break;
         }
     }
     return (intval($value));
    */
}