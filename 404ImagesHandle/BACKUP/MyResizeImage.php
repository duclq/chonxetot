<?php
use Eventviva\ImageResize;

/**
 * Created by Lê Đình Toản.
 * User: dinhtoan1905@gmail.com
 * Date: 5/1/2017
 * Time: 10:35 PM
 */
class MyResizeImage extends ImageResize
{
    public function flipImage($mode){
        imageflip($this->source_image, $mode);
    }
}