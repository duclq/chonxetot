<? 
set_time_limit(500);
header("Access-Control-Allow-Origin: *");
header('Cache-Control: no-cache, must-revalidate'); 
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
header('Content-type: application/json');

$rootPath = "../";
require_once($rootPath . "/vendor/eventviva/php-image-resize/lib/ImageResize.php");
require_once("functions.php");
require_once("upload.php");
use \Eventviva\ImageResize;

function reArrayFiles($file_post) {
    $arrayReturn = array();
    foreach($file_post as $photo){
      foreach($photo as $key => $file){
         if(is_array($file)){
            foreach($file as $key1 => $val1){
               $arrayReturn['photo_' . $key1][$key] = $val1;
            }
         }
      }
    }
    return $arrayReturn;
}

//$data  = array("images" => base64_encode(json_encode($this->getListImages())),"maxwidth" => $maxwidth,"maxheight" => $maxheight,"typepicture" => "tintuc");
if(!empty(reArrayFiles($_FILES))) $_FILES = reArrayFiles($_FILES);

$typePicture 				= "other";
$upload_name 				= 'picture';
$file_name 					= getValue("name","str","POST","");
$typePicture 				= getValue("typepicture","str","POST");
$arrayUrlImages 			= getValue("images","str","POST","");
$name 						= getValue('name', 'str', 'POST',"");
$maxwidth					= getValue('maxwidth', 'int', 'POST');
$maxheight					= getValue('maxheight', 'int', 'POST');
$arrayResult				= array();
$domain                 = "cdnxehoi.vatgia.vn";
$pathSave               = "../temp/";

$timeFile = time();
$upload_path = $pathSave . date("/Y/m/d/",$timeFile);
$extension_list = 'jpg,jpeg,png,gif';
$limit_size = 100000;
$quality		= 100;
if (!is_dir($upload_path)) mkdir($upload_path, 0777, true);
foreach($_FILES as $upload_name => $file){
	$upload = new \upload($upload_name, $upload_path, $extension_list, $limit_size, 1, $name,0,$timeFile);
	if ('' == $upload->common_error){
		$filename = $upload->file_name;
		$sExtension = substr($filename, (strrpos($filename, ".") + 1));
		$sExtension = strtolower($sExtension);
		try {
			$image = new ImageResize($upload_path . $filename);
			$image->resizeToWidth(2000);
			if($sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg"){
				$image->save($upload_path . $filename, null, 100);
	    		$log = shell_exec("/usr/local/bin/jpegoptim --max=85 --all-progressive " . escapeshellarg(realpath($upload_path . $filename)));
	    		//$arrayResult[$upload_name]["log"] 			= $log;
		   }else{
		   	$image->save($upload_path . $filename, null, 9);
		   }
			$arrayResult[$upload_name]["filename"] 			= $filename;
			$arrayResult[$upload_name]["url"] 		         = "https://" . $domain . "/full" . date("/Y/m/d/",$timeFile) . $filename;
			$arrayResult[$upload_name]["success"] 	      	= 1;
			$arrayResult[$upload_name]["width"] 			   = intval($image->getDestWidth());
			$arrayResult[$upload_name]["height"] 			   = intval($image->getDestHeight());
		} catch (Exception $e) {
			$arrayResult[$upload_name]["success"] 		      = 0;
		}
		
	}else{
		$arrayResult[$upload_name]["success"] 		= 0;
		$arrayResult[$upload_name]["error"] 		= $_FILES;
	}
}

echo json_encode($arrayResult);

