<?
//exit("abc");
// find . -type f -name "*.jpg" -exec jpegoptim {} \;
require_once("../vendor/eventviva/php-image-resize/lib/ImageResize.php");
require_once("functions.php");
require_once("MyResizeImage.php");
use \Eventviva\ImageResize;
$arraySizeAllow					= array(200,300,400,500);

$url 							= $_SERVER['REQUEST_URI'];
$sizeArr						= explode("/",$url);
$filetype 					= isset($sizeArr[1]) ? $sizeArr[1] : '';
$filename 					= end($sizeArr);
$width						= isset($sizeArr[2]) ? $sizeArr[2] : 100;
$timeFile					= intval($filename);
$timeFile					= intval($timeFile);
if($timeFile <= 0){
    if(preg_match('/^([a-zA-Z]+)([0-9]+)([.])/', $filename,$match)){
      if(isset($match[2])){
         $timeFile = intval($match[2]);
      }
    }
   
}
//kiem tra xem file ton tai chua neu chua thi copy vao
$file_source_path       = "../pictures/full" . date("/Y/m/d/",$timeFile) . $filename;
$file_source_temp       = "../temp/" . date("Y/m/d/",$timeFile) . $filename;
$sExtension = get_extension($filename);
if(!file_exists($file_source_path) && file_exists($file_source_temp)){
   if(!file_exists(dirname($file_source_path))) mkdir(dirname($file_source_path), 0777, true);
   copy($file_source_temp,$file_source_path);
   unlink($file_source_temp);
   if($sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg"){
    		$log = shell_exec("/usr/local/bin/jpegoptim --max=85 --all-progressive " . escapeshellarg(realpath($file_source_path)));
         @file_put_contents("../logs/fullsize.cfn",$log,FILE_APPEND);
   }
   if($filetype != "thumb"){
      showimage($file_source_path);
   }
}

/*
if($filetype == "full"){

    $path_thumb = "../pictures/full/" . date("/Y/m/d/",$timeFile) . $filename;
    if(!file_exists(dirname($path_thumb))) mkdir(dirname($path_thumb), 0777, true);
    if(file_exists($file_source_path)){
        list($img_width, $img_height, $type, $attr) = getimagesize($file_source_path);
        if($type < 4){
            $image = new MyResizeImage($file_source_path);

            $image->flipImage(IMG_FLIP_HORIZONTAL);
            $image->resizeToWidth($img_width - 2);
            if($type == 2){
                $image->save($path_thumb, null, 100);
            }else{
                $image->save($path_thumb, null, 9);
            }
            if($type == 2){
                $log = shell_exec("/usr/local/bin/jpegoptim --max=85 --all-progressive " . escapeshellarg(realpath($path_thumb)));
                @file_put_contents("../logs/thumb.cfn",$log,FILE_APPEND);
            }
        }else{
            copy($file_source_path,$path_thumb);
        }
        showimage($path_thumb);
    }

}
//*/
if($filetype == "thumb"){
      
      $path_thumb = "../pictures/thumb/" . $width . date("/Y/m/",$timeFile) . $filename;
      if(!file_exists(dirname($path_thumb))) mkdir(dirname($path_thumb), 0777, true);
      //if(!file_exists($file_source_path)) exit($file_source_path);
      list($img_width, $img_height, $type, $attr) = getimagesize($file_source_path);
      if($type < 4) {
          $image = new MyResizeImage($file_source_path);
          //$image->flipImage(IMG_FLIP_HORIZONTAL);
          $image->resizeToWidth($width);

          if ($type == 2) {
              $image->save($path_thumb, null, 100);
          } else {
              $image->save($path_thumb, null, 9);
          }
          if ($sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg") {
              $log = shell_exec("/usr/local/bin/jpegoptim --max=85 --all-progressive " . escapeshellarg(realpath($path_thumb)));
          }
      }else{
            copy($file_source_path,$path_thumb);
        }
		showimage($path_thumb);
}