<?
require_once('inc_security.php');

$list 	= new fsDataGird($field_id,$field_name,translate_text('Danh sách dòng xe'));

$veh_name 			= getValue('veh_name','str','GET', "", 1);
$iCat		 			= getValue("iCat");
$sql 	= "1";
if($veh_name != "")  $sql 	= "veh_name LIKE '%" . $veh_name . "%'";

$menu = new menu();
$listAll = $menu->getAllChild("vehicles_cxt", "veh_id", "veh_parent_id", $iCat, $sql . " ", "veh_id,veh_name,veh_rewrite,veh_parent_id,veh_md5,veh_date_create,veh_logo","veh_name ASC","");

$list->addSearch(translate_text("Tên xe"),"veh_name","text","",$veh_name);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<div class="listing">
	<? /*------------------------------------------------------------------------------------------------*/ ?>
	<?=template_top(translate_text("Danh sách danh mục"), $list->urlsearch())?>
	<?
	if(!is_array($listAll)) $listAll = array();

	?>
	<table class="table table-bordered table-striped" width="100%" bordercolor="<?=$fs_border?>">
		<tr>
			<td width="5" class="bold">Chọn</td>
			<td class="bold" width="2%" nowrap="nowrap" align="center">Lưu</td>
			<td class="bold" width="2%" nowrap="nowrap" align="center">Ảnh</td>
			<td class="bold" ><?=translate_text("Tên danh mục")?></td>
			<td class="bold" ><?=translate_text("Tên rewrite")?></td>
			<td class="bold" align="center" width="16">Sửa</td>
			<td class="bold" align="center" width="16">Xóa</td>
		</tr>
		<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" id="form_listing" enctype="multipart/form-data">
		<input type="hidden" name="iQuick" value="update">
		<?

		$i=0;
		$cat_type = '';
		foreach($listAll as $key=>$row){
			$i++;
			?>
			<tr>
				<td>
					<input type="checkbox" name="record_id[]" id="record_<?=$row["veh_id"]?>_<?=$i?>" value="<?=$row["veh_id"]?>">
				 </td>
				<td width="2%" nowrap="nowrap" align="center"><img src="<?=$fs_imagepath?>save.gif" border="0" style="cursor:pointer" onClick="document.form_listing.submit()" alt="Save"></td>
				<td nowrap="nowrap" align="center">
					<?
					if($row['veh_logo'] != ""){
						echo '<img height="50" src="/data/vehicles/' . $row['veh_logo'] . '">';
					}
					?>
				</td>
				<td nowrap="nowrap">
					<?
					for($j=0;$j<$row["level"];$j++) echo "--";
					?>
					<input type="text" style="width: 90%;" name="veh_name<?=$row["veh_id"];?>" id="veh_name<?=$row["veh_id"];?>" onKeyUp="check_edit('record_<?=$row["veh_id"]?>_<?=$i?>')" value="<?=$row["veh_name"];?>" class="form-control" size="50">
				</td>
				<td nowrap="nowrap">
					<?
					for($j=0;$j<$row["level"];$j++) echo "--";
					?>
					<input type="text" style="width: 90%;" name="veh_rewrite<?=$row["veh_id"];?>" id="veh_rewrite<?=$row["veh_id"];?>" onKeyUp="check_edit('record_<?=$row["veh_id"]?>_<?=$i?>')" value="<?=$row["veh_rewrite"];?>" class="form-control" size="50">
				</td>
				
				<td align="center" width="16"><a class="text" href="edit.php?record_id=<?=$row["veh_id"]?>&returnurl=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="EDIT" border="0"></a></td>
				<td align="center" width="16"><a class="text" href="delete.php?record_id=<?=$row["veh_id"]?>&returnurl=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>delete.gif" alt="DELETE" border="0"></a></td>
			</tr>
		<? } ?>
		</form>
		</table>
<?=template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</div>
</body>
</html>
