<?
$module_id 	= 13;

$fs_table            = "vehicles_cxt";
$field_id            = "veh_id";
$field_name          = "veh_name";
$fs_fieldupload      = "veh_logo";
$fs_filepath         = $_SERVER['DOCUMENT_ROOT'].'/data/vehicles/';
$fs_extension        = "gif,jpg,jpe,jpeg,png,swf";
$fs_filesize         = 2048;
$width_small_image   = 200;
$height_small_image  = 270;
$width_normal_image  = 270;
$height_normal_image = 270;
$fs_insert_logo      = 0;

//check security...
require_once("../../resource/security/security.php");
checkLogged();
if (checkAccessModule($module_id) != 1){
	header("location: ../deny.htm");
	exit();
}

$arrayParent = array(0 => "- Chọn -");
$db_parent = new db_query("SELECT * FROM vehicles_cxt WHERE veh_parent_id = 0");
while ($row_parent = mysqli_fetch_assoc($db_parent->result)) {
	$arrayParent[$row_parent['veh_id']] = $row_parent['veh_name'];
}
unset($db_parent);
?>