<?
$module_id 	= 13;

$fs_table            = "rewrites";
$field_id            = "rew_id";
$field_name          = "rew_title";
$fs_fieldupload      = "rew_picture";
$fs_filepath         = $_SERVER['DOCUMENT_ROOT'].'/data/rewrites/';
$fs_extension        = "gif,jpg,jpe,jpeg,png,swf";
$fs_filesize         = 2048;
$width_small_image   = 200;
$height_small_image  = 270;
$width_normal_image  = 270;
$height_normal_image = 270;
$fs_insert_logo      = 0;

//check security...
require_once("../../resource/security/security.php");
checkLogged();
if (checkAccessModule($module_id) != 1){
	header("location: ../deny.htm");
	exit();
}

$arrayParent = array(0 => "- Chọn -");
$db_parent = new db_query("SELECT * FROM rewrites WHERE rew_parent = 0");
while ($row_parent = mysqli_fetch_assoc($db_parent->result)) {
	$arrayParent[$row_parent['rew_id']] = $row_parent['rew_title'];
}
unset($db_parent);
?>