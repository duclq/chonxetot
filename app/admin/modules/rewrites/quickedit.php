<?
require_once("inc_security.php");
//check quyền them sua xoa
checkAddEdit("edit");
$returnurl 		= base64_decode(getValue("returnurl","str","GET",base64_encode("listing.php")));
$field_id		= "cat_id";

//Khai bao Bien
$errorMsg = "";
$iQuick = getValue("iQuick","str","POST","");
if ($iQuick == 'update'){
	$record_id = getValue("record_id", "arr", "POST", "");
	if($record_id != ""){
		for($i=0; $i<count($record_id); $i++){
			$errorMsg="";
			//Call Class generate_form();
			$myform = new generate_form();
			//Loại bỏ chuc nang thay the Tag Html
			$myform->removeHTML(0);
			$veh_name			= getValue("veh_name" . $record_id[$i],"str","POST","");
			$veh_rewrite	= getValue("veh_rewrite" . $record_id[$i],"str","POST","");
			if($veh_rewrite == "") $veh_rewrite 	= removeTitle($veh_name);
			$cat_order			= getValue("cat_order" . $record_id[$i],"int","POST",0);

			//Insert to database
			$myform->add("veh_name","veh_name",0,1,"",1,"Bạn chưa nhập tên danh mục",0,"");
			$myform->add("veh_rewrite","veh_rewrite",0,1,"",0,"",0,"");

			//Add table
			$myform->addTable($fs_table);
			$errorMsg .= $myform->checkdata();
			if($errorMsg == ""){
				$db_ex = new db_execute($myform->generate_update_SQL("veh_id",$record_id[$i]));
				unset($db_ex);
			}
		}
	}
	echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
	echo "Đang cập nhật dữ liệu !";
	redirect($returnurl);
}
?>