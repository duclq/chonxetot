<?
require_once('inc_security.php');

$list 	= new fsDataGird($field_id,$field_name,translate_text('Danh sách dòng xe'));

$rew_title 			= getValue('rew_title','str','GET', "", 1);
$iCat		 			= getValue("iCat");
$sql 	= "1";
if($rew_title != "")  $sql 	= "rew_title LIKE '%" . $rew_title . "%'";


$menu = new menu();
$listAll = $menu->getAllChild("rewrites", "rew_id", "rew_parent", $iCat, $sql . " ", "rew_id,rew_picture,rew_title,rew_noaccent,rew_rewrite,rew_md5,rew_parent,rew_param","rew_title ASC","");

$list->addSearch(translate_text("Tên xe"),"rew_title","text","",$rew_title);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<div class="listing">
	<? /*------------------------------------------------------------------------------------------------*/ ?>
	<?=template_top(translate_text("Danh sách danh mục"), $list->urlsearch())?>
	<?
	if(!is_array($listAll)) $listAll = array();

	?>
	<table class="table table-bordered table-striped" width="100%" bordercolor="<?=$fs_border?>">
		<tr>
			<td width="5" class="bold">Chọn</td>
			<td class="bold" width="2%" nowrap="nowrap" align="center">Lưu</td>
			<td class="bold" width="2%" nowrap="nowrap" align="center">Ảnh</td>
			<td class="bold" ><?=translate_text("Tên danh mục")?></td>
			<td class="bold" ><?=translate_text("Tên rewrite")?></td>
			<td class="bold" align="center" width="16">Sửa</td>
			<td class="bold" align="center" width="16">Xóa</td>
		</tr>
		<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" id="form_listing" enctype="multipart/form-data">
		<input type="hidden" name="iQuick" value="update">
		<?

		$i=0;
		$cat_type = '';
		foreach($listAll as $key=>$row){
			$i++;
			?>
			<tr>
				<td>
					<input type="checkbox" name="record_id[]" id="record_<?=$row["rew_id"]?>_<?=$i?>" value="<?=$row["rew_id"]?>">
				 </td>
				<td width="2%" nowrap="nowrap" align="center"><img src="<?=$fs_imagepath?>save.gif" border="0" style="cursor:pointer" onClick="document.form_listing.submit()" alt="Save"></td>
				<td nowrap="nowrap" align="center">
					<?
					if($row['rew_picture'] != ""){
						echo '<img height="50" src="/data/vehicles/' . $row['rew_picture'] . '">';
					}
					?>
				</td>
				<td nowrap="nowrap">
					<?
					for($j=0;$j<$row["level"];$j++) echo "--";
					?>
					<input type="text" style="width: 90%;" name="rew_title<?=$row["rew_id"];?>" id="rew_title<?=$row["rew_id"];?>" onKeyUp="check_edit('record_<?=$row["rew_id"]?>_<?=$i?>')" value="<?=$row["rew_title"];?>" class="form-control" size="50">
				</td>
				<td nowrap="nowrap">
					<?
					for($j=0;$j<$row["level"];$j++) echo "--";
					?>
					<input type="text" style="width: 90%;" name="rew_rewrite<?=$row["rew_id"];?>" id="rew_rewrite<?=$row["rew_id"];?>" onKeyUp="check_edit('record_<?=$row["rew_id"]?>_<?=$i?>')" value="<?=$row["rew_rewrite"];?>" class="form-control" size="50">
				</td>
				
				<td align="center" width="16"><a class="text" href="edit.php?record_id=<?=$row["rew_id"]?>&returnurl=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="EDIT" border="0"></a></td>
				<td align="center" width="16"><a class="text" href="delete.php?record_id=<?=$row["rew_id"]?>&returnurl=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>delete.gif" alt="DELETE" border="0"></a></td>
			</tr>
		<? } ?>
		</form>
		</table>
<?=template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</div>
</body>
</html>
