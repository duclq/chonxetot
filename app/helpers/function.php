<?php
/**
 * Created by Lê Đình Toản.
 * User: dinhtoan1905@gmail.com
 * Date: 4/1/2017
 * Time: 12:26 AM
 */
function sqlIn($listid,$field){
    $arrList = explode(",",$listid);
    $min = 0;
    $max = 0;
    if(count($arrList) == 1){
        return "$field = " . intval($listid);
    }
    foreach($arrList as $id){
        if($min == 0) $min =  ($id - 1);
        if($id < $min && $id > 0){
            $min = ($id - 1);
        }
        if($id > $max && $id > 0){
            $max = $id+1;
        }
    }
    return " $field IN(" . convert_list_to_list_id($listid) . ") AND $field > $min AND $field < $max";
}

function formatFeedFacebook($result){
    $arrayReturn = [];
    $result = arrayVal(arrGetVal("data",$result));
    foreach($result as $row){
        if($row["type"] != "photo") continue;
        $full_picture = arrGetVal("full_picture",$row);
        $message = nl2br(trim(convertToUnicode(removeEmoji(arrGetVal("message",$row,"")))));
        $pictures = arrayVal(arrGetVal("attachments.data.0.subattachments.data",$row));
        foreach($pictures as $key => $pic){
            $pictures[$key] = arrGetVal("media.image",$pic);
        }
        $link = arrGetVal("link",$row);
        $arrTemp = ["id" => $row["id"],"total_pictures"=> count($pictures),"link" => $link,"message" => $message,"pictures" => $pictures,"picture" => $full_picture];
        $arrayReturn[] = $arrTemp;
    }
    return $arrayReturn;
}

function checkSentEmail($email){
    $sem_md5 = md5(trim($email));
    $sem_length = strlen(trim($email));
    $db_select = new db_query("SELECT sem_length FROM send_email WHERE sem_length = $sem_length AND sem_md5 = '$sem_md5' LIMIT 1");
    if($row = mysqli_fetch_assoc($db_select->result)){
        return true;
    }else{
        $db_ex = new db_execute("INSERT INTO send_email(sem_md5,sem_length) VALUES ('$sem_md5',$sem_length)");
        return false;
    }
}

function getSphinxIndexClassified($cla_id = 0){
    $index = 'xehoi_rt_classifieds';

    $isLocal = config('app.env');
    if($isLocal != 'development'){
        $key_index = '';
        if($cla_id > 0) {
            $key_index = $cla_id % 4;
            $index .= "_" . $key_index;
        }
    }
    return $index;
}

/**
 * Trường hợp sử thì thêm các biến sau để bỏ qua chính nó
 */
function createRewriteNotExists($name_rewrite,$table  = "",$id_value = 0,$param_get = array(),$iCat = 0,$iCit = 0, $iDis = 0, $iWard = 0, $iStreet = 0,$otherTitle = "",$updateCount = 0, $rewrite_set= '', $rew_parent = 0){

    $rewrite = cleanRewriteAccent($name_rewrite);
    if($rewrite_set != '') $rewrite = $rewrite_set;
    $rew_md5 = md5($rewrite);
    $iCat = intval($iCat);
    $iCit = intval($iCit);
    $iDis = intval($iDis);
    $iWard = intval($iWard);
    $iProj = (int) arrGetVal("iProj",$param_get,0);
    $iStreet = intval($iStreet);
    $db_select = new \db_query("SELECT * FROM rewrites WHERE rew_md5 = '$rew_md5' LIMIT 1");
    if($row = mysqli_fetch_assoc($db_select->result)){
        if($updateCount == 1 && $row["rew_total_result"] <= 0){
            $db_ex = new \db_execute("UPDATE rewrites SET rew_total_result = rew_total_result+1 WHERE rew_id = " . $row["rew_id"]);
            unset($db_ex);
        }
        return $row;
    }else{
        $rew_total_result = 1;
        $rew_title = ($otherTitle != "") ? replaceMQ($otherTitle) : replaceMQ($name_rewrite);
        $rew_rewrite = $rewrite;
        $rew_param   = bdsEncode($param_get);
        $rew_date    = time();
        $rew_table   = replaceMQ($table);
        $rew_id_value = intval($id_value);
        $rew_count_word = getCountWordRewrite($rewrite);
        $rew_length = strlen($rewrite);
        $rew_noaccent = replaceMQ(removeAccent($rew_title));
        $db_ex = new \db_execute_return();
        $rew_parent = intval($rew_parent);
        $rew_id = $db_ex->db_execute("INSERT INTO rewrites(rew_title,rew_noaccent,rew_rewrite,rew_md5,rew_param,rew_date,rew_table,rew_id_value,rew_count_word,rew_cat_id,rew_cit_id,rew_dis_id,rew_ward_id,rew_street_id,rew_proj_id,rew_length,rew_total_result, rew_parent)
                                             VALUES('$rew_title','$rew_noaccent','$rew_rewrite','$rew_md5','$rew_param',$rew_date,'$rew_table',$rew_id_value,$rew_count_word,$iCat,$iCit, $iDis,$iWard, $iStreet,$iProj,$rew_length,$rew_total_result, $rew_parent)");



        return ["rew_id"=>$rew_id,"rew_rewrite" => $rew_rewrite];
    }
}

function checkLinkExists($url){
    $url = trim($url);
    $che_length = strlen($url);
    $che_url_md5 = md5(mb_strtolower($url,"UTF-8"));
    $db_select = new \db_query("SELECT che_length FROM checklink WHERE che_url_md5 = '$che_url_md5' AND che_length = $che_length LIMIT 1");
    $sql_check = "SELECT che_length FROM checklink WHERE che_url_md5 = '$che_url_md5' AND che_length = $che_length LIMIT 1";

    if($row = mysqli_fetch_assoc($db_select->result)){
        saveLog("checklink.cfn",'[TRUE]' . $sql_check . '|' . $url);
        $che_length = intval($row['che_length']);
        if($che_length <= 0) {
            $db_ex = new \db_execute("DELETE FROM checklink WHERE che_url_md5 = '$che_url_md5'");
            unset($db_ex);
        }
        return true;
    }else{
        if($che_length > 0){
            $db_ex = new \db_execute("INSERT IGNORE INTO checklink(che_url_md5,che_length) VALUES('$che_url_md5',$che_length)");
        }
        unset($db_ex);
    }
    return false;
}


function formatAddress($string){
    $string = convertToUnicode($string);
    $string = str_replace("q.","Quận ",$string);
    $string = str_replace("Q.","Quận ",$string);
    $string = str_replace("tp.","Thành phố ",$string);
    $string = str_replace("TP.","Thành phố ",$string);
    $string = str_replace("t.","Tỉnh ",$string);
    $string = str_replace("h.","Huyện ",$string);
    for($i = 1; $i < 25; $i++){
        $string = str_replace("q$i","Quận $i",$string);
        $string = str_replace("p$i","Phường $i",$string);
    }

    $string = str_replace("Tp.HCM","Thành phố Hồ Chí Minh",$string);
    $string = str_replace("TP HCM","Thành phố Hồ Chí Minh",$string);
    $string = str_replace("TP.HCM","Thành phố Hồ Chí Minh",$string);
    $string = str_replace("tp.hcm","Thành phố Hồ Chí Minh",$string);
    $string = str_replace("tp hcm","Thành phố Hồ Chí Minh",$string);
    $string = str_replace("hcm","Hồ Chí Minh",$string);
    $string = str_replace("KĐT","Khu đô thị",$string);
    $string = str_replace("kđt","Khu đô thị",$string);
    $string = str_replace("tp.hn","Thành phố Hà Nội",$string);
    $string = str_replace("sài gòn","Thành phố Hồ Chí Minh",$string);
    $string = str_replace("  "," ",$string);
    $string = str_replace("  ","",$string);
    //echo $string . '<hr>';
    return $string;
}

function getUserCreateIfNotExif($name,$email,$key_phone,$arrayPhone, $vg_use_id = 0){

    $vg_use_id = intval($vg_use_id);
    $phone = format_login_phone($key_phone);
    $arraySqlWhere = array();
    foreach($arrayPhone as $val){
        $val = format_login_phone($val);
        if($phone === false) $phone = $val;
        $arraySqlWhere[$val] = "'" . replaceMQ($val) . "'";
    }
    if(!isset($arraySqlWhere[$phone])) $arraySqlWhere[$phone] = "'" . replaceMQ($phone) . "'";
    if(trim($email) == "" && trim($phone) == "") return 0;
    $sql = '';
    if(trim($email) != "") $sql = "use_email = '" . replaceMQ($email) . "'";
    if(trim($phone) != "") $sql .= (($sql != "") ? " OR " : "") . " use_loginphone = '" . replaceMQ($phone) . "'";
    if($sql == "") return 0;

    $use_id = 0;
    $db_select = new db_query("SELECT use_id FROM users WHERE $sql  LIMIT 1","USE_SLAVE");
    if(mysqli_num_rows($db_select->result) <= 0) $db_select = new db_query("SELECT use_id FROM users WHERE $sql  LIMIT 1");
    if($row = mysqli_fetch_assoc($db_select->result)){
        $use_id = $row["use_id"];
    }

    //neu khong co ket qua thi search trong bang users_phone xem sao
    if(count($arraySqlWhere) > 1){
        $db_select = new db_query("SELECT usp_use_id FROM users_phone WHERE usp_phone IN(" . implode(",",$arraySqlWhere) .  ")  LIMIT 1");
        if($row = mysqli_fetch_assoc($db_select->result)){
            $use_id = $row["usp_use_id"];
        }
    }

    //neu chua co thi insert vao
    /*if($use_id == 0){
        $password = md5(rand(111111111,999999999));
        $password = md5($password);
        $db_ex = new db_execute_return();
        $loginname = ($email == "") ? $phone : $email;
        $use_id = $db_ex->db_execute("INSERT INTO users(use_loginname,use_name,use_password,use_email,use_phone,use_active)
                                                VALUES('" . replaceMQ($loginname) . "','" . replaceMQ($name) . "','" . replaceMQ($password) . "','" . replaceMQ($email) . "','" . replaceMQ($phone) . "',1,$vg_use_id)");
    }*/

    $sqlInsert = array();

    if($use_id > 0){
        $usp_date = time();
        foreach($arraySqlWhere as $key=>$val){
            $sqlInsert[$key] = "($use_id,$val,$usp_date)";
        }
        if(!empty($sqlInsert)){
            $db_ex = new db_execute("INSERT IGNORE INTO users_phone(usp_use_id,usp_phone,usp_date) VALUES " . implode(",",$sqlInsert));
        }
    }

    return $use_id;
}



/**
 * [getDiffText description]
 * @param  [type] $from text from
 * @param  [type] $to   text to
 * @param  [type] $type  0 - paragraph | 1 - sentence | 2 - word | 3 - character
 * @return [type]       [description]
 */
function getDiffText($from, $to, $type = 2){
    $time     = microtime(true);
    $memory   = memory_get_usage();
    $from_len = mb_strlen($from, 'UTF-8');
    $to_len   = mb_strlen($to, 'UTF-8');

    $granularityStacks = array(
        FineDiff::$paragraphGranularity,
        FineDiff::$sentenceGranularity,
        FineDiff::$wordGranularity,
        FineDiff::$characterGranularity
    );

    $diff           = new FineDiff($from, $to, $granularityStacks[$type]);
    $edits          = $diff->getOps();

    // var_dump($edits);die;//
    // $exec_time      = sprintf('%.3f sec', gettimeofday(true) - $start_time);
    // $rendered_diff  = $diff->renderDiffToHTML();
    // $rendering_time = sprintf('%.3f sec', gettimeofday(true) - $start_time);

    // $arrText = array();
    $opcodes     = array();
    $copy_len    = 0;
    $delete_len  = 0;
    $insert_len  = 0;
    $replace_len = 0;
    $copy        = 0;
    $delete      = 0;
    $insert      = 0;
    $replace     = 0;

    if ( $edits !== false ) {

        $offset = 0;
        foreach ( $edits as $edit ) {
            $n      = $edit->getFromLen();
            // $text   = mb_substr($from, $offset, $n, 'UTF-8');
            // $length = mb_strlen($text, 'UTF-8');

            if ( $edit instanceof FineDiffCopyOp ) {
                $state    = 'copy';
                $text     = mb_substr($from, $offset, $n, 'UTF-8');
                $copy_len += mb_strlen($text, 'UTF-8');

            }else if ( $edit instanceof FineDiffDeleteOp ) {
                $state = 'delete';
                $text     = mb_substr($from, $offset, $n, 'UTF-8');
                if ( strcspn($text, " \n\r") === 0 ) {
                    $text = str_replace(array("\n","\r"), array('\n','\r'), $text);
                }
                $delete_len += mb_strlen($text, 'UTF-8');

            }else if ( $edit instanceof FineDiffInsertOp ) {
                $state      = 'insert';
                $text       = mb_substr($edit->getText(), 0, $edit->getToLen(), 'UTF-8');
                $insert_len += mb_strlen($text, 'UTF-8');

            }else /* if ( $edit instanceof FineDiffReplaceOp ) */ {
                $state       = 'replace';

                // delete
                $text     = mb_substr($from, $offset, $n, 'UTF-8');
                if ( strcspn($text, " \n\r") === 0 ) {
                    $text = str_replace(array("\n","\r"), array('\n','\r'), $text);
                }
                $delete_len  += mb_strlen($text, 'UTF-8');
                $replace_len += $delete_len;

                // insert
                $text        = mb_substr($from, $offset, $n, 'UTF-8');
                $insert_len  += mb_strlen($text, 'UTF-8');
            }

            $opcodes[] = array('state' => $state, 'value' => $text);
            $offset += $n;
        }

        // $opcodes = implode("", $opcodes);
        // $opcodes_len = sprintf('%d bytes (%.1f %% of &quot;To&quot;)', $opcodes_len, $to_len ? $opcodes_len * 100 / $to_len : 0);
        $copy    = ($from_len > 0) ? $copy_len * 100 / $from_len : 0;
        $delete  = ($from_len > 0) ? $delete_len * 100 / $from_len : 0;
        $insert  = ($from_len > 0) ? $insert_len * 100 / $from_len : 0;
        $replace = ($from_len > 0) ? $replace_len * 100 / $from_len : 0;
    }

    return array(
        'meta'    => array(
            'time'   => microtime(true) - $time,
            'memory' => memory_get_usage() - $memory,
        ),
        'opcode'  => $opcodes,
        'copy'    => $copy,
        'delete'  => $delete,
        'insert'  => $insert,
        'replace' => $replace
    );
}

/**
 * Created by Lê Đình Toản
 * User: dinhtoan1905@gmail.com
 * Date: ${DATE}
 * Time: ${TIME}
 * @param $title
 * @param $teaser
 * @param int $minPercent
 */
function checkDiffClassified($title, $teaser, $minPercent = 60){
    $textSearch = cut_string($title,200,"");
    $sql_check_title = "SELECT id FROM xehoi_rt_classifieds WHERE MATCH('@(cla_title) \"" . cleanKeywordSearch($textSearch) . "\"/4') LIMIT 10";
    $db_sphinx = new db_sphinx($sql_check_title);
    while($sphinx = mysqli_fetch_assoc($db_sphinx->result)){
        $db_select = new db_query("SELECT cla_teaser FROM classifieds WHERE cla_id = " . $sphinx["id"] . " LIMIT 1");
        if($cla = mysqli_fetch_assoc($db_select->result)){
            $result = getDiffText(cleanKeywordSearch($cla["cla_teaser"]), cleanKeywordSearch($teaser));

            if($result["copy"] > $minPercent) return $result;
        }
        unset($db_select);
    }
    return false;
}

function insertProjectTemp($row){
    $proj_bds_id = intval(arrGetVal("id",$row,0));
    $proj_name = replaceMQ(arrGetVal("name",$row,0));
    $proj_lat = doubleval(arrGetVal("lat",$row,0));
    $proj_lng = doubleval(arrGetVal("lng",$row,0));
    $proj_cats= arrGetVal("cats",$row,[]);
    $proj_cats = replaceMQ(implode(",",$proj_cats));
    $db_select = new db_query("SELECT proj_id FROM projects WHERE proj_bds_id = " . $proj_bds_id . " LIMIT 1");
    if(mysqli_num_rows($db_select->result) <= 0){
        $db_ex = new db_execute_return();
        $proj_id  = $db_ex->db_execute("INSERT INTO projects(proj_bds_id,proj_name,proj_lat,proj_lng,proj_cats)
                                            VALUES($proj_bds_id,'$proj_name',$proj_lat,$proj_lng,'$proj_cats')");
        if($proj_id > 0){
            $db_ex = new db_execute("INSERT INTO projects_description(proj_proj_id) VALUES($proj_id)");
        }
        return false;
    }else{
        return true;
    }
    unset($db_select);

}

function showCategorySelectBox($current_id = 0, $fiel_name = "pro_category_id"){
    $listAll = array();
    $db_select 				= new db_query("SELECT cat_id,cat_name,cat_type,cat_has_child,cat_parent_id FROM categories_multi WHERE  cat_active = 1");
    while($cat = mysqli_fetch_assoc($db_select->result)){
        $listAll[$cat["cat_parent_id"]][$cat["cat_id"]] = $cat;
    }
    $string = '<select class="form_control" title="Danh mục" id="' . $fiel_name . '" name="' . $fiel_name . '" size="1">';
    $string .= '<option style="color: #3467b0;" value="0">Chọn danh mục</option>';
    foreach($listAll[0] as $cat){
        if(isset($listAll[$cat["cat_id"]])){
            $string .= '<optgroup label="' . $cat["cat_name"] . '" style="color: #d4004c;">';
            foreach($listAll[$cat["cat_id"]] as $cat1){
                if(isset($listAll[$cat1["cat_id"]])){
                    $string .= '<optgroup label="-- ' . $cat1["cat_name"] . '"  style="color: #d4004c;">';
                    foreach($listAll[$cat1["cat_id"]] as $cat2){
                        if(isset($listAll[$cat2["cat_id"]])){
                            $string .= '<optgroup label="' . $cat2["cat_name"] . '"  style="color: #d4004c;"></optgroup>';
                        }else{

                            $string .= '<option style="color: #3467b0;" ' . (($current_id == $cat2["cat_id"]) ? "selected" : '') .' value="' . $cat2["cat_id"] .'">&nbsp;&nbsp;' . $cat2["cat_name"] . '</option>';
                        }
                    }
                    $string .= '</optgroup>';
                }else{
                    $string .= '<option style="color: #3467b0;" ' . (($current_id == $cat1["cat_id"]) ? "selected" : '') .' value="' . $cat1["cat_id"] . '">' . $cat1["cat_name"] . '</option>';
                }
            }
            $string .= '</optgroup>';
        }else{
            $string .= '<option style="color: #3467b0;" ' . (($current_id == $cat["cat_id"]) ? "selected" : '') .' value="' . $cat["cat_id"] . '">&nbsp;&nbsp;&nbsp;' . $cat["cat_name"] . '</option>';
        }
    }
    $string .= '</select>';
    return $string;
}

function _debug_private($data = []){
    if (in_array(app("user")->u_id, config('user_right')) && $_SERVER['SERVER_NAME'] == 'localhost'){
        //print_r($data);
        //_debug($data);
    }
}

function dataTechnicalNormal(){
    $arr_technical = [
        '_3J9IfJCUXTV7Nne-3wMbVG' => [
            'code' => 'brand',
            'name' => 'Hãng'
            , 'value' => ''
        ],
        'Pv3_Dz1t8np10JwKkJsiA' => [
            'code' => 'model',
            'name' => 'Xe'
            , 'value' => ''
        ],
        'modelSub' => [
            'code' => '',
            'name' => 'Phiên bản'
            , 'value' => ''
        ],
        '_9HAKKMYgqH6qx3AqwapC4' => [
            'code' => 'productionDate',
            'name' => 'Năm Sx'
            , 'value' => ''
        ],
        'I0-3o28StAgFg3xkplTX9' => [
            'code' => '',
            'name' => 'Km đã đi'
            , 'value' => ''
        ],
        '_37LAHfRw2Bu6XcBkTrskHM' => [
            'code' => '',
            'name' => 'Tình trạng'
            , 'value' => ''
        ],
        '_3WJqImUjHoAAGCSpNa3aln' => [
            'code' => '',
            'name' => 'Hộp số'
            , 'value' => ''
        ],
        '_14BvoidU0Mv0NofUMn5Oa9' => [
            'code' => '',
            'name' => 'Nhiên liệu'
            , 'value' => ''
        ],
        '_14aKF2_vir3C5PsPiyA_Ca' => [
            'code' => '',
            'name' => 'Nhập khẩu'
            , 'value' => ''
        ],
        'GvKmVbpwnqqbp68z05Gz4' => [
            'code' => '',
            'name' => 'Dáng xe'
            , 'value' => ''
        ],
        '_3KgU8ZIfBF3n6SERQe-dRH' => [
            'code' => 'vehicleSeatingCapacity',
            'name' => 'Số ghế'
            , 'value' => ''
        ],
        'colorRbg' => [
            'code' => '',
            'name' => 'Màu sắc'
            , 'value' => ''
        ],
        'doorNumber' => [
            'code' => '',
            'name' => 'Số cửa'
            , 'value' => ''
        ],
        'priceUoctinh' => [
            'code' => '',
            'name' => 'Chi phí ước tính lăn bánh',
            'value' => ''
        ]
    ];

    return $arr_technical;
}

function dataTechnicalParamAll(){
    $technical_all = [
        'tech_show' => [
            'name' => 'Thông số nhanh',
            'data' => []
        ],
        'base' => [
            'name' => 'Thông số cơ bản',
            'data' => []
        ],
        'extendinfo' => [
            'name' => 'Tiện nghi',
            'data' => []
        ],
        'techinfo' => [
            'name' => 'Thông số kỹ thuật',
            'data' => []
        ],
        'protect' => [
            'name' => 'An toàn',
            'data' => []
        ],
        'photo' => [
            'name' => 'Hình ảnh',
            'data' => []
        ],
        'price' => [
            'name' => "Giá",
            'data' => []
        ],
        'seller' => [
            'name' => 'Thông tin người bán',
            'data' => []
        ]
    ];

    return $technical_all;
}