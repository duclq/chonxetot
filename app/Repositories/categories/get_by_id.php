<?php
use App\Models\Categories\Category;

$categories_multi = new Category();
$catId = isset($input['cat']) ? $input['cat'] : 0;
$category = $categories_multi->findByID($catId);

return [
    'vars' => [
        'category' => $category ? $category->toArray() : array()
    ]
];