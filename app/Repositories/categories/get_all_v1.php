<?php
/**
 * Created by ntdinh1987.
 * User: ntdinh1987
 * Date: 12/5/16
 * Time: 10:46 AM
 */

use App\Models\Categories\Category;

$categories = (new Category())->useCollection()->select_all();

return [
    'vars' => [
        'listRootCategories' => $categories
    ]
];