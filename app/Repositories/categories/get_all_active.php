<?php

use App\Models\Categories\Category;

$categories = (new Category())->getCategories([
    'type' => 'PRODUCT'
]);

return [
    'vars' => [
        'categories' => $categories
    ]
];