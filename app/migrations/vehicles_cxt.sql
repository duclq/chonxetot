/*
 Navicat Premium Data Transfer

 Source Server         : Locahost
 Source Server Type    : MySQL
 Source Server Version : 100119
 Source Host           : localhost:3306
 Source Schema         : crawler_all

 Target Server Type    : MySQL
 Target Server Version : 100119
 File Encoding         : 65001

 Date: 21/11/2018 22:37:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vehicles_cxt
-- ----------------------------
DROP TABLE IF EXISTS `vehicles_cxt`;
CREATE TABLE `vehicles_cxt`  (
  `veh_id` int(11) NOT NULL AUTO_INCREMENT,
  `veh_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `veh_rewrite` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `veh_md5` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `veh_parent_id` tinyint(1) NULL DEFAULT 0,
  `veh_date_create` int(11) NULL DEFAULT NULL,
  `veh_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`veh_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 238 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of vehicles_cxt
-- ----------------------------
INSERT INTO `vehicles_cxt` VALUES (1, 'Audi', 'audi', '4d9fa555e7c23996e99f1fb0e286aea8', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (2, 'Audi Q2', 'audi-q2', '4ddf247405580ed5856a5115c530e773', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (3, 'Audi Q8', 'audi-q8', 'ccbedec390ee2504e2ee80573ac890ba', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (4, 'Audi A1', 'audi-a1', '68df7157e19d8ad8f96614e58744a60a', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (5, 'Audi A3', 'audi-a3', '7751da3e320d6cb60ff86fe5bf40ede2', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (6, 'Audi A4', 'audi-a4', '3f211c449655d43b28f80f5bee66fb7e', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (7, 'Audi A6', 'audi-a6', '3940419bf57e93907f233a8df9cfafdf', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (8, 'Audi A7', 'audi-a7', 'efe12d39e093e099817e1dd2574b833b', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (9, 'Audi A8', 'audi-a8', 'c3597dee354f8b56e3cb691ead5ac41d', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (10, 'Audi Q3', 'audi-q3', '85545106a9515d9520997c7a00933ade', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (11, 'Audi Q5', 'audi-q5', '0e6cae652deab94286ac9d5436c92d2d', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (12, 'Audi Q7', 'audi-q7', '23d5e502f1cd948547164017f2fcb637', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (13, 'Audi TT', 'audi-tt', 'd253edcd1cedd91f34f9750b8e38db48', 1, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (14, 'BMW', 'bmw', '71913f59e458e026d6609cdb5a7cc53d', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (15, 'BMW M3', 'bmw-m3', '97c8a51b1cc4937bf5427c9df7c41f54', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (16, 'BMW M4', 'bmw-m4', '62c061ee5ea17eb87b1a5f42c29ff67a', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (17, 'BMW M6', 'bmw-m6', '6447a7f5a862d4cf085a0f1756f72118', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (18, 'BMW Series 1', 'bmw-series-1', 'dc82ce32fbfa339cff05ad428b671288', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (19, 'BMW Series 2', 'bmw-series-2', 'b43b5725600df1826e4283c08efcf280', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (20, 'BMW Series 3', 'bmw-series-3', '736f161af59e47b10c870da1d9034ea7', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (21, 'BMW Series 5', 'bmw-series-5', 'eb381b96c52f94118ebe9fb02613ed31', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (22, 'BMW Series 7', 'bmw-series-7', 'e80db8f83bb02b9dd6e9300e1302be21', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (23, 'BMW X2', 'bmw-x2', 'c0c1d43444ca69bcb0cc9945d1ae61eb', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (24, 'BMW X3', 'bmw-x3', '7bf037f9f8b2e5acaf8ebe52ea9b5e6b', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (25, 'BMW X4', 'bmw-x4', '8cae238644ab219265c5f841fa750db8', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (26, 'BMW X5', 'bmw-x5', 'c91a78b0361d489d285927ae945b2763', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (27, 'BMW X6', 'bmw-x6', 'a05fa4cd70294a90272180e0d6f6c87c', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (28, 'BMW Z4', 'bmw-z4', '6ffd08fb1be71caa06ec0b870d34c2ab', 14, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (29, 'Chevrolet', 'chevrolet', 'bf54fecf8f926be7d84e6ac87950f0b0', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (30, 'Chevrolet Trailblazer', 'chevrolet-trailblazer', 'a3f680b8364f7b7d7c156571a06787d1', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (31, 'Chevrolet Aveo / Gentra', 'chevrolet-aveo-gentra', '3eb2d04602ca5fb5e2e05d9182f2f5a1', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (32, 'Chevrolet Captiva', 'chevrolet-captiva', 'e246c0bdc569c3785b7c86582806e28a', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (33, 'Chevrolet Colorado', 'chevrolet-colorado', '152cb4e7057daf8a67ae447ad473306b', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (34, 'Chevrolet Cruze / Lacetti', 'chevrolet-cruze-lacetti', '9b775d4711e9caa9af7656a602a6646b', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (35, 'Chevrolet Orlando', 'chevrolet-orlando', 'aabb935e6cabec539244d199cb9ca916', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (36, 'Chevrolet Spark / Matiz', 'chevrolet-spark-matiz', 'bdc7da83ab926f076682b9e32a840e59', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (37, 'Chevrolet Trax', 'chevrolet-trax', '083f86eca047d1cc92188529fc2fca20', 29, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (38, 'Ford', 'ford', '80ca06abfa1a5104af9a770f485dad07', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (39, 'Ford Explorer', 'ford-explorer', '1468b468945ab1770f6bc53d70ce4ff7', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (40, 'Ford Ranger', 'ford-ranger', '67c0ba5440cbeeb3637e711a927c5b5f', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (41, 'Ford Ranger Raptor', 'ford-ranger-raptor', '42eec75c43164df95d3deb115b6ef290', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (42, 'Ford Transit', 'ford-transit', 'afb64819878c29b6659e7b4064bc32cc', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (43, 'Ford EcoSport', 'ford-ecosport', 'c1adbe32a47782743910f815d7cc8181', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (44, 'Ford Escape', 'ford-escape', '37b0dee5e899d28115396e4098254c97', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (45, 'Ford Everest', 'ford-everest', 'af706692421fc3e413f3dd1327c70608', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (46, 'Ford Fiesta', 'ford-fiesta', '7248fc8c0d2c59da9a28248cbe218d2c', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (47, 'Ford Focus', 'ford-focus', '1945d2a4a9289f564946b4cf61e1dfef', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (48, 'Ford Mondeo', 'ford-mondeo', '97078681ea36c56c51a1985063f5af5b', 38, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (49, 'Honda', 'honda', '2ab3343875e56dc0a15cbb6a98570cf2', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (50, 'Honda Brio', 'honda-brio', 'da546feb77a079b3979c354bd6cd12a5', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (51, 'Honda HR-V', 'honda-hr-v', 'a0a50c97ceff468a89a922841aed509f', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (52, 'Honda Jazz', 'honda-jazz', 'a0621a3cb4da6998c1a5dbbaa9941d95', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (53, 'Honda Accord', 'honda-accord', '740e43e7e39d113cdb4a70f8777f2487', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (54, 'Honda City', 'honda-city', '45200bc3aad93bde8bf9b65d69ce9642', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (55, 'Honda Civic', 'honda-civic', '18d02693982ed89412ce151f440f93ea', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (56, 'Honda CR-V', 'honda-cr-v', '94c39950bc53696fd05db893a7649ea0', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (57, 'Honda Odyssey', 'honda-odyssey', '35967ff70fddc71472b17887bb173bb0', 49, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (58, 'Hyundai', 'hyundai', 'df98d265fc651a91faa09d4e82f096cc', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (59, 'Hyundai Kona', 'hyundai-kona', '0d7dc88bcb4b32fdebc13307685a5fb6', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (60, 'Hyundai Accent', 'hyundai-accent', '99d976ebde468b2dfb1745e5535e0342', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (61, 'Hyundai Avante', 'hyundai-avante', '014c034dba67fcab8199958d822d290b', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (62, 'Hyundai Creta', 'hyundai-creta', 'c892e6e57cee7fe39223f446f69de0fc', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (63, 'Hyundai Elantra', 'hyundai-elantra', '46dc16be55063ba047dc2c2262fd0db8', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (64, 'Hyundai EON', 'hyundai-eon', 'e1885a73fe2c0ba0b9ddc0853dadb4dd', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (65, 'Hyundai Equus', 'hyundai-equus', '6dd56d33e29772938f435fcf52ef3260', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (66, 'Hyundai Genesis', 'hyundai-genesis', '5e2c4e9879668eb73187766926a7cd56', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (67, 'Hyundai Getz', 'hyundai-getz', '391fa1f9d0d9bdbd0d1097eadd9df4f6', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (68, 'Hyundai i10', 'hyundai-i10', 'f2ff55d0e52180a88d75b22643b51c7d', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (69, 'Hyundai i20', 'hyundai-i20', '6aa188e673d0bbcacfb57077bf75ff81', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (70, 'Hyundai i20 Active', 'hyundai-i20-active', '5546bc4d0d0b466ea6b0787659097ab6', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (71, 'Hyundai i30', 'hyundai-i30', 'd399e79dacccd683b1cd6c8c1668c8fb', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (72, 'Hyundai Santafe', 'hyundai-santafe', '1b992352df3676432f643e5a7672f7dc', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (73, 'Hyundai Sonata', 'hyundai-sonata', '4d7bb3d79f826e44f87854a659eae7ca', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (74, 'Hyundai Tucson', 'hyundai-tucson', 'faf4af7733e7138bbb0c457874d261ed', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (75, 'Hyundai Veloster', 'hyundai-veloster', '66b8af285c59bf75b8315f57fbb08504', 58, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (76, 'KIA', 'kia', 'ab99f8f1c87bb63eee8ddc8688ce329f', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (77, 'KIA Cadenza', 'kia-cadenza', 'f8cb87c9fa6053a2b94ff1f28c1c2b0c', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (78, 'KIA Carens', 'kia-carens', 'f3ea9130eb383af7a878f4474e4234c5', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (79, 'KIA Carnival', 'kia-carnival', '061aeff209681a4044ec4384b888076d', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (80, 'KIA Forte / Cerato', 'kia-forte-cerato', 'b379e680251949bbfe8660fde884e6df', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (81, 'KIA K3', 'kia-k3', '9fbe97bc93a37b3f6dd727fa5e9eb3f6', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (82, 'KIA Koup', 'kia-koup', 'c2cb89229f268bf891cb237484af29dd', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (83, 'KIA Morning / Picanto', 'kia-morning-picanto', '4513be3a1f3a8cbd1f717311827ee6d8', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (84, 'KIA Optima', 'kia-optima', '5d1d14251bffa70df7ef325bf0f25cd5', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (85, 'KIA Rio', 'kia-rio', 'bb8663ed0ea6fd2efb170b6d332daa09', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (86, 'KIA Rondo', 'kia-rondo', 'ff3c98b0dc69ede65d61b33ab633db78', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (87, 'KIA Sedona', 'kia-sedona', '962855e79a905c3b0109f914d91ea6a3', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (88, 'KIA Sorento', 'kia-sorento', 'b004644de2a50b92d14a1e955dc06ff3', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (89, 'KIA Soul', 'kia-soul', 'bf66538f50b163fe11db2abb056cbdac', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (90, 'KIA Sportage', 'kia-sportage', 'c6dd7694ef38a000b446c40ed08aa156', 76, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (91, 'Lexus', 'lexus', '2a972164e25bd335636cbd451a40b853', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (92, 'Lexus LX570', 'lexus-lx570', 'effec620848c2bd38d0b4ae12eb55e70', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (93, 'Lexus ES', 'lexus-es', '4f8984cb35bcab79134bb536b5b99665', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (94, 'Lexus GS', 'lexus-gs', '59c968c06bb89efd4a06debe95b3b45a', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (95, 'Lexus GX', 'lexus-gx', '8269c19690f66f3c55d1abe2f3a2b59b', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (96, 'Lexus LS', 'lexus-ls', '9f6b12f7910345e7d7fe9fe71eac25f0', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (97, 'Lexus LX', 'lexus-lx', '6bc320fd393df36e5020ed2e84050c53', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (98, 'Lexus NX', 'lexus-nx', 'd2b89e747d3eb440be0166dabcbe846d', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (99, 'Lexus RX', 'lexus-rx', '2417613c05d59f995e9169dc43793196', 91, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (100, 'Mercedes-Benz', 'mercedes-benz', '251cfa24a30764c18eb10fe36aefe06d', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (101, 'Mercedes-Benz GLC 200', 'mercedes-benz-glc-200', 'e06dbfa78eee5476747b3d3f5e8b55c1', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (102, 'Mercedes-Benz A-Class', 'mercedes-benz-a-class', 'c49f26d27ee6e21de43a950d76cc7cba', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (103, 'Mercedes-Benz C-class', 'mercedes-benz-c-class', '3426a00fcb83d8e4da043a7588fb29a8', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (104, 'Mercedes-Benz CL-class', 'mercedes-benz-cl-class', 'c6b8c32567ae13751b873922b1d9ce43', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (105, 'Mercedes-Benz CLA Class', 'mercedes-benz-cla-class', 'c172770af115ef93349cf875e62cc3b5', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (106, 'Mercedes-Benz CLS-class', 'mercedes-benz-cls-class', 'ab2c4cbab1e301c80db6d6320d6c7dac', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (107, 'Mercedes-Benz E-class', 'mercedes-benz-e-class', '28357972b8d4329a5cfa5e4eb22b6a98', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (108, 'Mercedes-Benz G-Class', 'mercedes-benz-g-class', '0dee4c90db94b38f439e0f6b07580889', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (109, 'Mercedes-Benz GL-class', 'mercedes-benz-gl-class', 'a09202961c2664af2e5d3a5c0ff75f6b', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (110, 'Mercedes-Benz GLA-Class', 'mercedes-benz-gla-class', 'c9ba97b87cac9b3c27a128cb8e1f795c', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (111, 'Mercedes-Benz GLC-Class', 'mercedes-benz-glc-class', 'e07056e11910fba52cb0f5422d5aa571', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (112, 'Mercedes-Benz GLE-Class', 'mercedes-benz-gle-class', '96ca569eb5f73f137305d1bab669d214', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (113, 'Mercedes-Benz GLK-class', 'mercedes-benz-glk-class', 'c464fdc3e3d4f59cc4d9bee98f6a672d', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (114, 'Mercedes-Benz R-class', 'mercedes-benz-r-class', '45c7f2619bbf9674a133f2bc87ff943a', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (115, 'Mercedes-Benz S-class', 'mercedes-benz-s-class', 'e8fbf12e5145e8c4c5bdbaa08ac5f67b', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (116, 'Mercedes-Benz SLK-class', 'mercedes-benz-slk-class', '95073c8fb71a7f3d112c40be54b8452f', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (117, 'Mercedes-Benz V-class', 'mercedes-benz-v-class', '11a95288a12db4c6c01b1d17d3913029', 100, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (118, 'Mazda', 'mazda', 'b7d0308621091afe8588693966373120', 0, 1542814569, NULL);
INSERT INTO `vehicles_cxt` VALUES (119, 'Mazda 2', 'mazda-2', '556eae3ec90f7f44ff2abaa33e30a4ce', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (120, 'Mazda 3', 'mazda-3', 'b98351370d9a64aa6381decf3f2deb62', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (121, 'Mazda 6', 'mazda-6', '77d7ff9f4f2cd08e88779a93b534a4a5', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (122, 'Mazda BT-50', 'mazda-bt-50', '6edc8cbaf7e757d073cd5d312e69120b', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (123, 'Mazda CX-3', 'mazda-cx-3', 'cd52019b5ac1e521e96c087f7d15a509', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (124, 'Mazda CX-5', 'mazda-cx-5', '3054db0314d1647f4d4fc2f574ad43a6', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (125, 'Mazda CX-9', 'mazda-cx-9', 'cf5cc5bf7177f775175714e76d0c60be', 118, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (126, 'Mitsubishi', 'mitsubishi', 'f815564f1fb658fbc1d8df4eb059ea77', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (127, 'Mitsubishi Attrage', 'mitsubishi-attrage', '22b621efc2dd945b7c927a2392b10b7c', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (128, 'Mitsubishi Mirage', 'mitsubishi-mirage', '0835f69fc6a126e784ae15ebd7908615', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (129, 'Mitsubishi Outlander', 'mitsubishi-outlander', 'e829bd76a44cdf4c1151db052037a124', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (130, 'Mitsubishi Outlander Sport', 'mitsubishi-outlander-sport', '1897e98c85677abbc83e462e673db52b', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (131, 'Mitsubishi Pajero', 'mitsubishi-pajero', 'f24a0b80566fb908d98b8701826f2d4c', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (132, 'Mitsubishi Pajero Sport', 'mitsubishi-pajero-sport', '929ca5edd4f4d180fce62189a98ebfbf', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (133, 'Mitsubishi Triton', 'mitsubishi-triton', '3e631d47ad2670619368414724c34a68', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (134, 'Mitsubishi Xpander', 'mitsubishi-xpander', 'cbb5cdb69790fa03cd0ee716c94b7753', 126, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (135, 'Nissan', 'nissan', '8928603cd5f39e8583cf8becbc180bd2', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (136, 'Nissan 370Z', 'nissan-370z', '5db483cb17c222f59537f05176721e44', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (137, 'Nissan Juke', 'nissan-juke', 'fd2641fcad05bd5d5a5ac511fa80a7ab', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (138, 'Nissan Navara', 'nissan-navara', '42b80239937d9abff8fcf039b624de7d', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (139, 'Nissan Sunny', 'nissan-sunny', 'f81c1fe6e6fb2109ea590add68694c3b', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (140, 'Nissan Teana', 'nissan-teana', '7c61d9d301d4f0be164ac08800d7dc1b', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (141, 'Nissan Terra', 'nissan-terra', 'da1455519a6074d42cb58e0323a4c12d', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (142, 'Nissan X-Trail', 'nissan-x-trail', '966d8940aea4de89dbce33bc0a845f31', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (143, 'Peugeot', 'peugeot', '9f6cf82deb99289e14aea9dcc24e54cb', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (144, 'Peugeot 208', 'peugeot-208', '04387867f7c018496f10bb31aa42de65', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (145, 'Peugeot 3008', 'peugeot-3008', 'a496fdaceab0ee535c419857f6d6fbd6', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (146, 'Peugeot 308', 'peugeot-308', '385519d3564b61c9f937b8573d0351a2', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (147, 'Peugeot 408', 'peugeot-408', '110e6deba014b45c52650d52edc70612', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (148, 'Peugeot 508', 'peugeot-508', '4e70f33148b3904956e0e1c28d3ddfa0', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (149, 'Peugeot RCZ', 'peugeot-rcz', '845a29f8ffb3bced6fbac2f03176f6ae', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (150, 'Suzuki', 'suzuki', '101186a9a44bc0354ed997696a6aefba', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (151, 'Suzuki Celerio', 'suzuki-celerio', '31f51a28b828064cd6f9610ee79aa81a', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (152, 'Suzuki APV', 'suzuki-apv', 'a90222a2e9b7cbfdbbf53a76c794ec50', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (153, 'Suzuki Ciaz', 'suzuki-ciaz', '54068d52ee6ce4f5894e8e46add3da74', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (154, 'Suzuki Ertiga', 'suzuki-ertiga', 'dbd228cd9e7eab3f977c8e7fecf1bb0c', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (155, 'Suzuki Grand Vitara', 'suzuki-grand-vitara', '11070a817817ff81fb1be9be42eb73aa', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (156, 'Suzuki Swift', 'suzuki-swift', '114b029f76ade0787b2c3ca86c2db763', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (157, 'Suzuki Vitara', 'suzuki-vitara', 'e532fac2591d21430deba7c1b7d8263f', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (158, 'Toyota', 'toyota', '5166fd3fee463b7c9992293e84f098f0', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (159, 'Toyota Rush', 'toyota-rush', 'e74962e17257cfdf55ac36d506d0ad9b', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (160, 'Toyota Altis', 'toyota-altis', '023dfc99d46be552a0095f2b0ff9c87e', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (161, 'Toyota Camry', 'toyota-camry', 'b98d8b0169fa91fe657db59b02033390', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (162, 'Toyota Fortuner', 'toyota-fortuner', '446ceea2702378e2a436112b248adcd6', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (163, 'Toyota FT-86', 'toyota-ft-86', '81985fe8cf64646c01bae3c4c9adb010', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (164, 'Toyota Hilux', 'toyota-hilux', '80bd847e544d689820dd0562b8b1a6c8', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (165, 'Toyota Innova', 'toyota-innova', '5fb0bc01e7a65aab502d964039f49dd6', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (166, 'Toyota Land Cruiser', 'toyota-land-cruiser', '27f27e4e65fcb1a89de921fcde2de533', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (167, 'Toyota Land Cruiser Prado', 'toyota-land-cruiser-prado', '04acc18f910d47d6c4e4ee185e970296', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (168, 'Toyota Sienna', 'toyota-sienna', 'e578ab0bfdea66b36b500acf9ad72516', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (169, 'Toyota Vios', 'toyota-vios', '2046614d2d8a43d1f5cf75b9f6bc6f50', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (170, 'Toyota Yaris', 'toyota-yaris', '47376e88270fb5b1b38f10941055bd91', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (171, 'Toyota Hiace', 'toyota-hiace', '7ff0c4c5bb794c97171e83aa350c3f9c', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (172, 'Porsche', 'porsche', 'd599eae7a636d54c1c707514b1a76d77', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (173, 'Porsche 911', 'porsche-911', '2a819305340643e0bc93de065baa18a9', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (174, 'Porsche Boxster', 'porsche-boxster', '217f48d92a2d7e320f5e3b34ae0d9e15', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (175, 'Porsche Cayenne', 'porsche-cayenne', '61f8a67797a08c46d9409d60b5dd928f', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (176, 'Porsche Cayman', 'porsche-cayman', '9d0e4144ff4854cc2835ffc275804bf6', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (177, 'Porsche Macan', 'porsche-macan', 'b243e9aadc0f98da42690055070c9e42', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (178, 'Porsche Panamera', 'porsche-panamera', 'b163d82b21a023a32a6a80cc66de39e6', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (179, 'Subaru', 'subaru', '0b9c5120ea86155f02d81e6f2f90c900', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (180, 'Subaru BRZ', 'subaru-brz', '2adf688c7c71966807ddf5dd7efff9d7', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (181, 'Subaru Forester', 'subaru-forester', 'e9367feeb669562cfd723f2de396ad94', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (182, 'Subaru Legacy', 'subaru-legacy', 'b5447d8a1a7fd249444aa2f6f3373bbf', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (183, 'Subaru Levorg', 'subaru-levorg', 'b2cca52661a382c28ef1860bd0c63679', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (184, 'Subaru Outback', 'subaru-outback', '6e10126a7b4ce7b946634753f1772587', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (185, 'Subaru Tribeca', 'subaru-tribeca', '9b144c30ab6da2fa8992d536023845bb', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (186, 'Subaru WRX', 'subaru-wrx', '95a49b1d5b811b25021c07c902bb9c0f', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (187, 'Subaru XV', 'subaru-xv', '133ef19eb08b937ddc774efb0a7d9341', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (188, 'Renault', 'renault', '0fa9a61dcebb5cf6cf98a08d9ef8084b', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (189, 'Renault Clio RS', 'renault-clio-rs', 'ea0efe37e483d6cc925edcdb610705d7', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (190, 'Renault Duster', 'renault-duster', 'cc425f8296ddc6236bc512b78bb0a8cc', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (191, 'Renault Koleos', 'renault-koleos', '53d84532a62996410c07fea57a5f759d', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (192, 'Renault Latitude', 'renault-latitude', '847c925721969f522f99e69107cf88de', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (193, 'Renault Logan', 'renault-logan', '68c440369d2c3d1da417e9e6ddf0a12f', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (194, 'Renault Megane', 'renault-megane', '7335168ea88affaa2e7b0300c2f7bfe8', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (195, 'Renault Sandero', 'renault-sandero', '358fe79a7a49e8e008a56d8d30c97ba2', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (196, 'MINI', 'mini', '44c6c370fd1859325f7119e96a81584e', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (197, 'MINI 5-Door', 'mini-5-door', '83c2ff42065402e7db68824dae9d60c3', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (198, 'MINI Clubman', 'mini-clubman', '17b63b2c521b195400118fe8119c605a', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (199, 'MINI Cooper', 'mini-cooper', '97098aa7b2a407e8dafef5b70ec5fa89', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (200, 'MINI Countryman', 'mini-countryman', 'dc07e6842963da41747c7afe10b18d88', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (201, 'Infiniti', 'infiniti', '3a24904ae5256f6c04dd74b14382ef3b', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (202, 'Infiniti Q50', 'infiniti-q50', '45b4988153c05c6e3af6259ba1466659', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (203, 'Infiniti QX60', 'infiniti-qx60', '0ebb1fd091c981ddd552c45f15e8e036', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (204, 'Infiniti QX70', 'infiniti-qx70', '977a12f9f9d9f3bfdc677f3efcd9d791', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (205, 'Infiniti QX80', 'infiniti-qx80', '4e31d2323bd3f89ca5440b9208f92b7e', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (206, 'Volkswagen', 'volkswagen', '7bd9ba9c7afb18529ac23979f477ff77', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (207, 'Volkswagen Jetta', 'volkswagen-jetta', '7020a71e13e36f695bd947e7cc6274fe', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (208, 'Volkswagen Touageg', 'volkswagen-touageg', 'ae212625a538b84d362be3fbafa954d3', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (209, 'Volkswagen Beetle', 'volkswagen-beetle', '431ee5a0eb661be41dbec72bd45859bc', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (210, 'Volkswagen Passat', 'volkswagen-passat', '3ed3272e6e5697e058fdef0e2b9d08ea', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (211, 'Volkswagen Polo', 'volkswagen-polo', '69fd3ae64e29dffe2ab78e7c71957662', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (212, 'Volkswagen Scirocco', 'volkswagen-scirocco', '7246af0bf5fed833af84b8c5c78af64c', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (213, 'Volkswagen Tiguan', 'volkswagen-tiguan', '03af17c15d241ce4da5acb79852418e4', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (214, 'Volkswagen Touareg', 'volkswagen-touareg', '4b8e360837ae78d2abd0c1e26d34d0e3', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (215, 'Isuzu', 'isuzu', 'ce4a9e276c37e83dc7633c956fe06353', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (216, 'Isuzu D-Max', 'isuzu-d-max', '466bb39f456e61d4535f03df2f5e81c4', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (217, 'Isuzu mu-X', 'isuzu-mu-x', 'ae402c40ed2820367c084af5aa93e66a', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (218, 'Jaguar', 'jaguar', 'b94705c817b09f287e31606604e526ba', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (219, 'Jaguar F-Pace', 'jaguar-f-pace', '389da6d60dfedfd2c08ec6bafe56355e', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (220, 'Jaguar F-TYPE', 'jaguar-f-type', '6fe4549b07b793999395d45e2c35787d', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (221, 'Jaguar XE', 'jaguar-xe', '18eb54a25e5329d669b1626e21dfcf82', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (222, 'Jaguar XF', 'jaguar-xf', '4784535f051c01d16f78d5126fb67c77', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (223, 'Jaguar XJ', 'jaguar-xj', '1c93de75b5b6188b867172edcfe5a331', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (224, 'Maserati', 'maserati', 'c8e33a48e3b0fe1f27c5a2ca67c26161', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (225, 'Maserati Ghibli', 'maserati-ghibli', '09d66347efb5796b10aee1448f865d3c', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (226, 'Maserati Levante', 'maserati-levante', '53b9f21b2595da9eb76664134f258204', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (227, 'Land Rover', 'land-rover', 'd1c46dab8e44beefb8fa36eb3a9abf03', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (228, 'Land Rover All New Range Rover', 'land-rover-all-new-range-rover', '5d3bbf448a1f4ebf5a2d9ef1d1c05449', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (229, 'Land Rover All New Range Rover Sport', 'land-rover-all-new-range-rover-sport', '795106ba81bbde4dc4f8f919886e98b7', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (230, 'Land Rover Defender', 'land-rover-defender', 'fb563af54fa3694a4bc9faefcf116ac9', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (231, 'Land Rover Discovery', 'land-rover-discovery', 'b3ae7017704042a31aca5943633d224c', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (232, 'Land Rover Discovery Sport', 'land-rover-discovery-sport', 'fa669fd6f37be1b32824b673787c41c9', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (233, 'Land Rover Range Rover Evoque', 'land-rover-range-rover-evoque', '457d6718976b164b9d78e13e9512e645', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (234, 'Volvo', 'volvo', '3df7453748507d4ea82277d2d0420de5', 0, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (235, 'Volvo XC60', 'volvo-xc60', '2dd4b1160e0395b38225031828a61349', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (236, 'Volvo S90', 'volvo-s90', '93ac3a8853734c2a319a96ab5e921184', 127, 1542814570, NULL);
INSERT INTO `vehicles_cxt` VALUES (237, 'Volvo XC90', 'volvo-xc90', '34515598a894c2a91ef456ab08af53c4', 127, 1542814570, NULL);

SET FOREIGN_KEY_CHECKS = 1;
