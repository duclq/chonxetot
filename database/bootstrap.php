<?php
/**
 * Created by PhpStorm.
 * User: ntdinh1987
 * Date: 3/19/18
 * Time: 20:57
 */
use VatGia\LoadEnv;

LoadEnv::load(realpath(dirname(__FILE__) . '/../'));

class ColumnTypes
{
    const BIG_INTEGER = 'biginteger';
    const BINARY = 'binary';
    const BOOLEAN = 'boolean';
    const CHAR = 'char';
    const DATE = 'date';
    const DATETIME = 'datetime';
    const DECIMAL = 'decimal';
    const FLOAT = 'float';
    const INTEGER = 'integer';
    const VARCHAR = 'string';
    const STRING = 'string';
    const TEXT = 'text';
    const TIME = 'time';
    const TIMESTAMP = 'timestamp';
    const UUID = 'uuid';
    const ENUM = 'enum';
    const SET = 'set';
    const BLOB = 'blob';
}

class ColumnOptions
{
    const LIMIT = 'limit';
    const LENGTH = 'length'; //Alias of limit
    const DEFAULT = 'default';
    const NULL = 'null';
    const AFTER = 'after';
    const COMMENT = 'comment';

    //For decimal

    //Độ chính xác
    const PRECISION = 'precision';
    //Tỷ lệ
    const SCALE = 'scale';

    //enable or disable the unsigned option (only applies to MySQL)
    const SIGNED = 'signed';

    //For integer and big integer

    //enable or disable automatic incrementing
    const IDENTITY = 'identity';

    //For timestamp columns

    //set an action to be triggered when the row is updated (use with CURRENT_TIMESTAMP)
    const UPDATE = 'update';



}