<?
    require_once dirname(__FILE__) . '/../config.php';

    $sql = "SELECT * FROM vehicles_cxt";
    $db = new db_query($sql);

    $result = $db->fetch();

    $arr_parent = [];

    foreach ($result as $key_veh => $row_vehicle){
        $veh_id = intval($row_vehicle['veh_id']);
        $veh_parent_id = intval($row_vehicle['veh_parent_id']);
        if($veh_parent_id == 0) {
            $arr_parent[$veh_id] = $row_vehicle;
            $arr_parent[$veh_id]['child'] = [];
        }
    }


    foreach ($result as $key_veh => $row_vehicle){
        $veh_id = intval($row_vehicle['veh_id']);
        $veh_parent_id = intval($row_vehicle['veh_parent_id']);
        if($veh_parent_id > 0) {
            if(isset($arr_parent[$veh_parent_id])){
                $arr_parent[$veh_parent_id]['child'][$veh_id] = $row_vehicle;
            }
        }
    }



    $arr_rewrite = [];
    $prefix = "Đánh giá xe";
    foreach ($arr_parent as $row_veh){
        $id_parent = $row_veh['veh_id'];
        $title = $prefix . " " . $row_veh['veh_name'];
        $rewrite = cleanRewriteAccent($title);
        $md5_rewrite = md5($rewrite);
        $arr_rewrite[$md5_rewrite] = [
            'id'        => $id_parent,
            'title'     => $title,
            'rewrite'   => $rewrite,
            'parent'    => 0,
            'param'     => [
                'iVeh'      => $id_parent,
                'brand'     => $row_veh['veh_name'],
                'module'    => 'review'
            ],
            'child'     => []
        ];

        if(isset($row_veh['child']) && !empty($row_veh['child'])){
            foreach ($row_veh['child'] as $veh_child){
                $id_child = $veh_child['veh_id'];
                $title_child = $prefix . " " . $veh_child['veh_name'];
                $rewrite_child = cleanRewriteAccent($title_child);
                $md5_child = md5($rewrite_child);
                $arr_rewrite[$md5_rewrite]['child'][$md5_child] = [
                    'id'        => $id_child,
                    'title'     => $title_child,
                    'rewrite'   => $rewrite_child,
                    'parent'    => $id_parent,
                    'param'     => [
                        'iVeh'      => $id_child,
                        'brand'     => $veh_child['veh_name'],
                        'module'    => 'review'
                    ]
                ];
            }
        }
    }

    _debug($arr_rewrite);

    foreach ($arr_rewrite as $row_rewrite){
        $otherTitle = $row_rewrite['title'];
        $nameRewrite = $row_rewrite['rewrite'];
        $arrayData = $row_rewrite['param'];
        $id_parent = createRewriteNotExists($nameRewrite,"vehicle",0,$arrayData,$row_rewrite["id"],0,0,0,0,$otherTitle);
        $id_parent = $id_parent['rew_id'];
        $child = $row_rewrite['child'];
        if(!empty($child)){
            foreach ($child as $row_child) {
                $title_child = $row_child['title'];
                $nameRewrite_child = $row_child['title'];
                $arrayDataChild = $row_child['param'];
                $id_child = createRewriteNotExists($nameRewrite_child,"vehicle",0,$arrayDataChild,$row_child["id"],0,0,0,0,$title_child,0, '', $id_parent);
            }
        }
        unset($id_parent);
    }

?>