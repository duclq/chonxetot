<!DOCTYPE HTML>

<html>
<head>
	<title>Đánh Giá Xe - Tư Vấn Xe Ô tô từ cộng đồng & chuyên gia | chonxetot.com</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<link rel="icon" href="<?php echo asset('images/favicon.ico'); ?>" type="image/x-icon">
	<link rel="stylesheet" href="<?php echo asset('css/css_header.css'); ?>"/>

	<script type="text/javascript">
		var jQl={q:[],dq:[],gs:[],ready:function(a){"function"==typeof a&&jQl.q.push(a);return jQl},getScript:function(a,c){jQl.gs.push([a,c])},unq:function(){for(var a=0;a<jQl.q.length;a++)jQl.q[a]();jQl.q=[]},ungs:function(){for(var a=0;a<jQl.gs.length;a++)jQuery.getScript(jQl.gs[a][0],jQl.gs[a][1]);jQl.gs=[]},bId:null,boot:function(a){"undefined"==typeof window.jQuery.fn?jQl.bId||(jQl.bId=setInterval(function(){jQl.boot(a)},25)):(jQl.bId&&clearInterval(jQl.bId),jQl.bId=0,jQl.unqjQdep(),jQl.ungs(),jQuery(jQl.unq()), "function"==typeof a&&a())},booted:function(){return 0===jQl.bId},loadjQ:function(a,c){setTimeout(function(){var b=document.createElement("script");b.src=a;document.getElementsByTagName("head")[0].appendChild(b)},1);jQl.boot(c)},loadjQdep:function(a){jQl.loadxhr(a,jQl.qdep)},qdep:function(a){a&&("undefined"!==typeof window.jQuery.fn&&!jQl.dq.length?jQl.rs(a):jQl.dq.push(a))},unqjQdep:function(){if("undefined"==typeof window.jQuery.fn)setTimeout(jQl.unqjQdep,50);else{for(var a=0;a<jQl.dq.length;a++)jQl.rs(jQl.dq[a]); jQl.dq=[]}},rs:function(a){var c=document.createElement("script");document.getElementsByTagName("head")[0].appendChild(c);c.text=a},loadxhr:function(a,c){var b;b=jQl.getxo();b.onreadystatechange=function(){4!=b.readyState||200!=b.status||c(b.responseText,a)};try{b.open("GET",a,!0),b.send("")}catch(d){}},getxo:function(){var a=!1;try{a=new XMLHttpRequest}catch(c){for(var b=["MSXML2.XMLHTTP.5.0","MSXML2.XMLHTTP.4.0","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP","Microsoft.XMLHTTP"],d=0;d<b.length;++d){try{a= new ActiveXObject(b[d])}catch(e){continue}break}}finally{return a}}};if("undefined"==typeof window.jQuery){var $=jQl.ready,jQuery=$;$.getScript=jQl.getScript};
		jQl.loadjQ('<?php echo asset('js/js_main.js'); ?>');
	</script>
</head>
<body>

	<!-- Wrapper -->
	<div id="cxt_wrapper">

		<!-- Header -->
		<header id="cxt_header">
			<div class="cxt_header_bar">
				<div class="cxt_container">
					<ul class="cxt_hb_main cxt_fl">
						<li><a href="#">Thông tin ô tô và xe máy Việt Nam</a></li>
						<li><a href="#">Hotline: <span class="hotline_number">0903.762.768</span></a></li>
						<li><a href="#">Tư vấn hỏi đáp</a></li>
					</ul>
					<ul class="cxt_hb_main cxt_fr">
						<li><a href="#" class="icon_facebook"><i class="flaticon-facebook"></i></a></li>
						<li><a href="#" class="icon_google"><i class="flaticon-google-plus-logo-on-black-background"></i></a></li>
					</ul>
					<div class="cxt_clear"></div>
				</div>
			</div>
			<div class="cxt_header_main">
				<div class="cxt_container">
					<ul>
						<li class="cxt_logo">
							<a href="/"><img src="<?php echo asset('images/logo.png'); ?>" alt=""></a>
						</li>
						<li class="cxt_search">
							<form class="cxts_form">
								<input type="text" name="search" placeholder="Tìm kiếm ...">
								<button type="submit" class="btn_search"><i class="flaticon-search"></i></button>
							</form>
						</li>
						<li class="cxt_login">
							<a href="#">
								<i class="flaticon-user"></i><br>
								<span>Đăng nhập</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="sticky">
				<div class="pagemenu">
					<div class="cxt_container">
						<ul>
							<li class="first activemenu">
								<a href="/" id="tc"><i class="flaticon-home"></i></a>
							</li>
							<li class="first" id="banxe">
								<a href="/ban-xe" data-key="/ban-xe">Mua bán ô tô</a>
							</li>

							<li>
								<a href="/salons" data-key="/salons">Salon ô tô</a>
							</li>
							<li>
								<a href="/tin-tuc" data-key="/tin-tuc">Tin tức</a>
								<ul class="sub-menu">
									<li><a href="/tin-tuc/thi-truong-o-to" data-key="/thi-truong-o-to">Thị trường ô tô</a></li>
									<li><a href="/tin-tuc/xe-moi" data-key="/xe-moi">Xe mới</a></li>
									<li><a href="/tin-tuc/su-kien" data-key="/su-kien">Sự kiện</a></li>
									<li><a href="/khuyen-mai/khuyen-mai-tren-ban-xe-hoi" data-key="/khuyen-mai-tren-ban-xe-hoi">Khuyến mại trên Banxehoi</a></li>
									<li><a href="/khuyen-mai/khuyen-mai-tu-cac-hang" data-key="/khuyen-mai-tu-cac-hang">Khuyến mại từ các hãng</a></li>
								</ul>
							</li>
							<li>
								<a href="/tin-tuc/gia-xe-o-to" data-key="/gia-xe-o-to">Giá xe ô tô</a>
							</li>
							<li>
								<a href="/danh-gia-xe" data-key="/danh-gia-xe">Đánh giá xe</a>

							</li>
							<li><a href="/so-sanh-xe" data-key="/so-sanh-xe">So sánh xe</a></li>
							<li>
								<a href="/kinh-nghiem" data-key="/kinh-nghiem">Kinh nghiệm</a>
								<ul class="sub-menu">
									<li><a href="/kinh-nghiem/kinh-nghiem-lai-xe" data-key="/kinh-nghiem-lai-xe">Kinh nghiệm lái xe</a></li>
									<li><a href="/kinh-nghiem/cham-soc-va-bao-duong-o-to" data-key="/cham-soc-va-bao-duong-o-to">Chăm sóc và bảo dưỡng ô tô</a></li>
									<li><a href="/kinh-nghiem/kinh-nghiem-mua-ban-xe" data-key="/kinh-nghiem-mua-ban-xe">Kinh nghiệm mua / bán xe</a></li>
								</ul>
							</li>
							<li>
								<a href="/tu-van-xe" data-key="/tu-van-xe">Tư vấn xe</a>
								<ul class="sub-menu">
									<li><a href="/tu-van-xe/tu-van-phap-luat" data-key="/tu-van-phap-luat">Tư vấn pháp luật</a></li>
									<li><a href="/tu-van-xe/bao-hiem-xe" data-key="/bao-hiem-xe">Bảo hiểm xe</a></li>
									<li><a href="/tu-van-xe/phong-thuy-xe" data-key="/phong-thuy-xe">Phong thủy xe</a></li>
								</ul>
							</li>
							<li>
								<a href="/o-to-360" data-key="/o-to-360">Ô tô 360</a>
								<ul class="sub-menu">
									<li><a href="/o-to-360/cong-nghe-moi" data-key="/cong-nghe-moi">Công nghệ mới</a></li>
									<li><a href="/o-to-360/kham-pha" data-key="/kham-pha">Khám phá</a></li>
									<li><a href="/o-to-360/lich-su-cac-hang-xe" data-key="/lich-su-cac-hang-xe">Lịch sử các hãng xe</a></li>
									<li><a href="/o-to-360/nguoi-dep-va-xe" data-key="/nguoi-dep-va-xe">Người đẹp và xe</a></li>
								</ul>
							</li>
							<li>
								<a href="/videos" data-key="/videos">Video ô tô</a>
								<ul class="sub-menu">
									<li><a href="/videos/danh-gia-xe" data-key="videos/danh-gia-xe">Video đánh giá</a></li>
									<li><a href="/videos/giao-thong" data-key="videos/giao-thong">Video giao thông</a></li>
									<li><a href="/videos/doc-va-la" data-key="videos/doc-va-la">Video độc - lạ</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</header>

		<div id="cxt_main">
			<div class="cxt_container">
				<div class="box_article_hot">
					<div class="bah_main">
						<div class="bah_item">
							<a href="#" class="article_link">
								<img src="<?php echo asset('images/hot_1.jpg'); ?>" alt="">
								<div class="article_info">
									<h3>Tinh Tế kết hợp với Samsung Việt Nam đã tổ chức nhiều mini game</h3>
									<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
									<span class="author_name">Hoàng Mỹ Linh</span>

									<span class="article_date"> - 12 giờ</span>
								</div>
							</a>
						</div>
					</div>
					<div class="bah_main">
						<div class="bah_item bah_medium">
							<a href="#" class="article_link">
								<img src="<?php echo asset('images/hot_2.jpg'); ?>" alt="">
								<div class="article_info">
									<h3>Giải thưởng game hay nhất ĐNÁ: Nhiều tác phẩm hay, phản ánh đúng</h3>
									<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
									<span class="author_name">Hoàng Mỹ Linh</span>

									<span class="article_date"> - 12 giờ</span>
								</div>
							</a>
						</div>
						<div class="bah_small">
							<div class="bah_item">
								<a href="#" class="article_link">
									<img src="<?php echo asset('images/hot_3.jpg'); ?>" alt="">
									<div class="article_info">
										<h3>Workshop quản lý ảnh nét từ chụp tới hậu kỳ - Hafoto</h3>
										<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
										<span class="author_name">Hoàng Mỹ Linh</span>

										<span class="article_date"> - 12 giờ</span>
									</div>
								</a>
							</div>
							<div class="bah_item">
								<a href="#" class="article_link">
									<img src="<?php echo asset('images/hot_3.jpg'); ?>" alt="">
									<div class="article_info">
										<h3>VinFast ra mắt xe tay ga điện Klara - pin Lithium</h3>
										<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
										<span class="author_name">Hoàng Mỹ Linh</span>
										<span class="article_date"> - 12 giờ</span>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>

				<div class="box_article_main">
					<div class="bam_item">
						<div class="bam_title">
							<h3>DANH MỤC</h3>
						</div>
						<div class="bam_category">
							<ul>
								<li>
									<a href="#">
										<img src="<?php echo asset('images/audi.jpg'); ?>" alt="">
										<span>Audi</span>
									</a>
								</li>
								<li>
									<a href="#">
										<img src="<?php echo asset('images/bentley.jpg'); ?>" alt="">
										<span>Bentley</span>
									</a>
								</li>
								<li>
									<a href="#">
										<img src="<?php echo asset('images/bmw.jpg'); ?>" alt="">
										<span>BMW</span>
									</a>
								</li>
								<li>
									<a href="#">
										<img src="<?php echo asset('images/ferrari.jpg'); ?>" alt="">
										<span>Ferrari</span>
									</a>
								</li>
								<li>
									<a href="#">
										<img src="<?php echo asset('images/porsche.jpg'); ?>" alt="">
										<span>Porsche</span>
									</a>
								</li>
							</ul>
						</div>

						<div class="bam_title">
							<h3>TIN ĐỌC NHIỀU</h3>
						</div>
						<div class="bam_popular">
							<ul>
								<li>
									<a href="#">
										<div class="bamp_image">
											<img src="<?php echo asset('images/list_small.jpg'); ?>" alt="">
										</div>
										<div class="bamp_title t_ov_4">Bộ đôi ca sĩ Ayano Mashiro và ASCA đem không khí anime...</div>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="bamp_image">
											<img src="<?php echo asset('images/list_small.jpg'); ?>" alt="">
										</div>
										<div class="bamp_title t_ov_4">Bộ đôi ca sĩ Ayano Mashiro và ASCA đem không khí anime...</div>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="bamp_image">
											<img src="<?php echo asset('images/list_small.jpg'); ?>" alt="">
										</div>
										<div class="bamp_title t_ov_4">Bộ đôi ca sĩ Ayano Mashiro và ASCA đem không khí anime...</div>
									</a>
								</li>
							</ul>
						</div>

						<div class="bam_title">
							<h3>CHỦ ĐỀ ĐANG HOT</h3>
						</div>
						<div class="bam_article_hot">
							<ul>
								<li class="bamah_big">
									<a href="#">
										<img src="<?php echo asset('images/medium.jpg'); ?>" alt="">
										<span class="t_ov_4">Đang dùng iphone 5s muốn tư vấn sử dụng loại cách để lên XS hoặc ...</span>
									</a>
								</li>
								<li class="bamah_big">
									<a href="#">
										<span class="t_ov_4">Đang dùng iphone 5s muốn tư vấn sử dụng loại cách để lên XS hoặc ...</span>
									</a>
								</li>
								<li class="bamah_big">
									<a href="#">
										<span class="t_ov_4">Đang dùng iphone 5s muốn tư vấn sử dụng loại cách để lên XS hoặc ...</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="bam_item">
						<div class="bam_title">
							<h3>CẬP NHẬT MỚI</h3>
						</div>
						<ul class="bam_list">
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
							<li class="bam_list_item">
								<a href="#">
									<div class="bam_img">
										<img src="<?php echo asset('images/list_img.jpg'); ?>" alt="">
									</div>
									<div class="bam_content">
										<div class="bamc_title">Một vòng Dell Gaming Village, nhiều sản phẩm và hoạt động vô cùng</div>
										<div class="bamc_teaser t_ov_4">Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện mang tên Dell Gaming Vilage. Đến với sự kiện này, người dùng có thể trải nghiệm. Sáng nay tại nhà thi đấu Hồ Xuân Hương số 2 Hồ Xuân Hương Quận 3. Delll đã kết hợp với các đại lý của mình để tổ chức sự kiện...</div>
										<div class="bamc_info">
											<img src="<?php echo asset('images/avatar.jpg'); ?>" alt="">
											<span class="author_name">Hoàng Mỹ Linh</span>

											<span class="article_date"> - 12 giờ</span>
										</div>
									</div>
								</a>
							</li>
						</ul>
						<div class="bam_load_more">
							<a href="javascript:;">Xem thêm</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer -->
		<footer id="cxt_footer">
			<div class="cxt_footer_top">
				<div class="cxt_container">
					<ul>
						<li><a href="#">Trang chủ</a></li>
						<li><a href="#">Tin mới</a></li>
						<li><a href="#">Liên hệ</a></li>
						<li><a href="#">Liên hệ quảng cáo</a></li>
					</ul>
				</div>
			</div>
			<div class="cxt_footer_main">
				<div class="cxt_container">
					<div class="cxt_fm_thumb">
						<div class="cxt_fm_item">
							<div class="cxt_fm_title">Hỗ trợ khách hàng</div>
							<ul class="cxt_fm_content">
								<li><a href="#">Quy định, chính sách</a></li>
								<li><a href="#">Trợ giúp</a></li>
								<li><a href="#">Liên hệ</a></li>
							</ul>
						</div>
						<div class="cxt_fm_item">
							<div class="cxt_fm_title">Tin tức đánh giá xe</div>
							<ul class="cxt_fm_content">
								<li><a href="#">Đánh giá xe Ford Ecosport 2018</a></li>
								<li><a href="#">Đánh giá xe Nissan X-Trail 2018</a></li>
								<li><a href="#">Đánh giá xe Ford Everest 2019</a></li>
							</ul>
						</div>
						<div class="cxt_fm_item">
							<div class="cxt_fm_title">Tin tức bán xe hơi</div>
							<ul class="cxt_fm_content">
								<li><a href="#">Báo giá dịch vụ</a></li>
								<li><a href="#">Khuyến mại từ các hãng</a></li>
								<li><a href="#">Cải tiến & tính năng mới</a></li>
							</ul>
						</div>
						<div class="cxt_fm_item">
							<div class="cxt_fm_title">Liên kết</div>
							<ul class="cxt_fm_content">
								<li><a href="#"><i class="flaticon-facebook"></i> Facebook</a></li>
								<li><a href="#"><i class="flaticon-google-plus-logo-on-black-background"></i> Google +</a></li>
								<li><a href="#"><i class="flaticon-instagram-logo"></i> Instagram</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cxt_footer_bottom">
				<div class="cxt_container">
					<p class="copyright"> 2018 © My Team <i class="flaticon-favorite-heart-button"></i> <a href="/">Chonxetot.com</a></p>
					<a href="javascript:;" class="go_top"></a>
				</div>
			</div>

		</footer>
	</div>

</body>
</html>